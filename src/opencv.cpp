/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <opencv.h>

#include <iostream>

/* write an OpenCV image buffer to a QPixmap */
QPixmap mattopixmap(Mat const &src)
{
	Mat tmp;

	cvtColor(src, tmp, COLOR_BGR2RGB);
	QImage dest((const unsigned char *) tmp.data, tmp.cols, tmp.rows,
			QImage::Format_RGB888);
	dest.bits();

	return QPixmap::fromImage(dest);
}

/* convert Mat to a QString to later reconvert using stringtomat() */
QString mattostring(Mat src)
{
	std::stringstream dest;

	/* write the size and type of Mat first as <rows, cols, type> */
	dest << '<' << src.rows << ',' << src.cols << ',' << src.type() << '>';

	/* then write the matrix */
	dest << src;

	return QString::fromStdString(dest.str());
}


/* convert a QString exported from mattostring() */
/* TODO use QString internally */
Mat stringtomat(QString _src)
{
	std::string src = _src.toStdString();
	std::string info = src.substr(src.find("<") + 1, src.find(">"));
	std::vector<int> matinfo;
	std::string data = src.substr(src.find("[") + 1);
	int row = 0, col = 0;
	std::string entry;
	Mat dest;

	/* restore the size and type of Mat (<rows, cols, type>) */
	for (char &c : info) {
		if (c == ',' || c == '>') {
			matinfo.push_back(stoi(entry));
			entry.clear();
		} else {
			entry.push_back(c);
		}
	}

	/*
	 * TODO assert if vector is actually filled
	 * not very likely, as this may only happen if somebody has tampered
	 * with the config file
	 */

	/* allocate the mat with the correct size and type */
	dest.create(matinfo[0], matinfo[1], matinfo[2]);

	/* parse the matrix string and write to Mat */
	for (unsigned int i = 0; i < data.length(); i++) {
		if (data[i] == ']' || data[i] == ';' || (data[i] == ',' &&
					data[i + 1] == ' ')) {
			dest.at<double>(row, col) = stod(entry);
			entry.clear();

			if (data[i] == ';') {
				row++;
				col = 0;
			} else {
				col++;
			}
		} else if (!isspace(data[i])) {
			entry.push_back(data[i]);
		}
	}

	return dest;
}

	/* XXX XXX XXX XXX REMOVE XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX */
/* attempt opening a camera and restoring its settings */
bool opencam(VideoCapture &cap, QSettings &settings, const QString key)
{
	/* avoid the use of groups for thread safety */

	if (settings.contains("camera/" + key + "/index"))
		cap.open(settings.value("camera/" + key + "/index").toInt());

	if (!cap.isOpened())
		return false;


	if (!settings.contains("camera/" + key + "/width") ||
			!settings.contains("camera/" + key + "/height") ||
			cap.get(CAP_PROP_BACKEND) != CAP_DSHOW)
		return true;

	cap.set(CAP_PROP_FRAME_WIDTH, settings.value("camera/" + key + "/width").toInt());
	cap.set(CAP_PROP_FRAME_HEIGHT, settings.value("camera/" + key + "/height").toInt());

	return true;
}
	/* XXX XXX XXX XXX REMOVE XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX */

/* find a chessboard pattern in the supplied image */
int corners_find(Mat &img, std::vector<Point2f> &points, int w, int h)
{
	const int flags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE |
			CALIB_CB_FAST_CHECK;
	Mat imggray;

	if (!findChessboardCorners(img, Size(w, h), points, flags))
		return 0;

	/* improve the found corners' coordinate accuracy */
	cvtColor(img, imggray, COLOR_BGR2GRAY);
	cornerSubPix(imggray, points, Size(11, 11), Size(-1, -1),
			TermCriteria(TermCriteria::EPS + TermCriteria::COUNT,
			30, 0.0001));

	return 1;
}

/* draw corners on the supplied image */
void corners_draw(Mat &img, std::vector<Point2f> &points, int w, int h)
{
	drawChessboardCorners(img, Size(w, h), Mat(points), true);
}

/* retrieve vector with object points */
std::vector<Point3f> getobj(int w, int h, int n)
{
	std::vector<Point3f> obj;

	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			obj.push_back(Point3f((double) j * n, (double) i * n,
						0));

	return obj;
}

/* convert rotation matrix to (axis-)angle representation and return angle */
/* FIXME this function hasn't been tested with real world data yet */
double calcrotation(Mat src)
{
	/*
	 * the first algorithm I came up with myself, but the second seems
	 * simpler. both yield the same output so far
	 */
#if 1
	Mat vec;

	/* convert rotation matrix to Rodrigues vector */
	Rodrigues(src, vec);

	/* calculate the angle */
	double theta = sqrt(pow(vec.at<double>(0, 0), 2) + pow(vec.at<double>(1,
					0), 2) + pow(vec.at<double>(2, 0), 2));

	/* convert to degrees and return */
	return theta * 180.0 / M_PI;
#else
	/*
	 * taken from
	 * https://github.com/opencv/opencv_contrib/blob/master/modules/surface_matching/src/c_utils.hpp#L240
	 */
	return acos(0.5 * (trace(src).val[0] - 1.0)) * 180.0 / M_PI;
#endif
}
