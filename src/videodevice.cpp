/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <videodevice.h>
#include <videobackend/opencv.h>

static VideoDeviceManager *backends[] = {
	new OpenCVVideoDeviceManager
};

static StaticVideoDevice static_left, static_right;

QList<VideoDevice *> videodevices()
{
	QList<VideoDevice *> devices;

	for (unsigned int i = 0; i < sizeof(backends) / sizeof(void *); i++)
		devices.append(backends[i]->enumerate());

	return devices;
}

/* attempt to open left and right camera streams, return true is successful */
bool opencams(VideoDevice *&leftvdev, VideoDevice *&rightvdev)
{
	QSettings settings;
	QList<VideoDevice *> devices;
	int leftn, rightn;

	/* attempt to retrieve video device index from settings */
	if (settings.contains("camera/left/index"))
		leftn = settings.value("camera/left/index").toInt();
	else
		goto fail;

	if (settings.contains("camera/left/index"))
		rightn = settings.value("camera/right/index").toInt();
	else
		goto fail;

	devices = videodevices();

	/* check if devices still exist */
	if (leftn > devices.size() - 1 || rightn > devices.size() - 1)
		goto fail;

	/* attempt to open the devices */
	leftvdev = videodevices()[leftn];
	if (!leftvdev->open()) {
		leftvdev = NULL;
		goto fail;
	}

	rightvdev = videodevices()[rightn];
	if (!rightvdev->open()) {
		leftvdev->close();
		leftvdev = NULL;
		rightvdev = NULL;
		goto fail;
	}

	return true;

fail:
	leftvdev = rightvdev = NULL;

	return false;
}

void loadimgs(VideoDevice *&leftvdev, Mat &leftsrc, VideoDevice *&rightvdev, Mat &rightsrc)
{
	static_left.load(leftsrc);
	static_right.load(rightsrc);

	leftvdev = (VideoDevice *) &static_left;
	rightvdev = (VideoDevice *) &static_right;
}
