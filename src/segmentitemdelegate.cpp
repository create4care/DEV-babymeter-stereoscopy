/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <segmentitemdelegate.h>

#include <main.h>

#include <QPainter>

#define PADDING		8

#define POINT_FONTSIZE	16
#define LENGTH_FONTSIZE	20

QSize SegmentItemDelegate::sizeHint(const QStyleOptionViewItem &option,
		const QModelIndex &index) const
{
	(void) index;

	/* calculate the height of the item by retrieving the largest font */
	QFont lengthfont = QFont(UI_FONT, std::max(POINT_FONTSIZE,
				LENGTH_FONTSIZE));

	QFontMetrics fm = QFontMetrics(lengthfont);

	QSize size = option.rect.size();
	size.rheight() = fm.height() + PADDING * 2;

	return size;
}

void SegmentItemDelegate::paint(QPainter *painter,
		const QStyleOptionViewItem &option, const QModelIndex &index)
		const
{
	if (!index.isValid())
		return;

	/*
	 * check if receiving segment information, call original function
	 * otherwise
	 */
	if (index.model()->columnCount() != 2) {
		QStyledItemDelegate::paint(painter, option, index);
		return;
	}

	painter->save();

	/* retrieve data from array */
	QString point = index.data(0).toString();
	QString length = index.sibling(index.row(), 1).data(0).toString();

	QFont font = option.font;
	QRect rect = {
		option.rect.left() + PADDING * 2,
		option.rect.top() + PADDING,
		option.rect.width() - PADDING * 4,
		option.rect.height() - PADDING
	};

	painter->setPen(Qt::white);

	/* draw the segment name */
	font.setPointSize(POINT_FONTSIZE);
	painter->setFont(font);
	painter->drawText(rect, Qt::AlignLeft | Qt::AlignVCenter, point);

	/* draw the length */
	font.setPointSize(LENGTH_FONTSIZE);
	painter->setFont(font);
	painter->drawText(rect, Qt::AlignRight | Qt::AlignVCenter, length);

	painter->restore();
}
