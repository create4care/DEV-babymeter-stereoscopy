/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <include/videobackend/opencv.h>

/* TODO implement threading to run the aquisition on a separate thread */

OpenCVVideoDevice::OpenCVVideoDevice(int n)
{
	name = tr("Device ") + QString::number(n);
	index = n;
}

int OpenCVVideoDevice::open()
{
	if (isOpen())
		return 1;

	videocap.open(index);

	if (!isOpen())
		return 0;

	QSettings settings;

#ifdef W_OS_WIN32
	if (!settings.contains("camera/" + key + "/width") ||
			!settings.contains("camera/" + key + "/height") ||
			videocap.get(CAP_PROP_BACKEND) != CAP_DSHOW)
		return true;

	videocap.set(CAP_PROP_FRAME_WIDTH, settings.value("camera/" + key + "/width").toInt());
	videocap.set(CAP_PROP_FRAME_HEIGHT, settings.value("camera/" + key + "/height").toInt());
#endif

	return 1;
}

void OpenCVVideoDevice::close()
{
	if (isOpen())
		videocap.release();

	VideoDevice::close();
}

bool OpenCVVideoDevice::isOpen()
{
	return videocap.isOpened();
}

void OpenCVVideoDevice::loadFrame()
{
	if (isOpen())
		videocap >> image;
}

OpenCVVideoDeviceManager::OpenCVVideoDeviceManager()
{
	VideoCapture videocap;

	for (int i = 0;; i++) {
		videocap.open(i);

		if (!videocap.isOpened())
			break;

		devices.append((VideoDevice *) new OpenCVVideoDevice(i));

		videocap.release();
	}
}

QList<VideoDevice *> OpenCVVideoDeviceManager::enumerate()
{
	return devices;
}
