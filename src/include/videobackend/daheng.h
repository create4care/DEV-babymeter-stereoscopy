/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <videodevice.h>

#include "../daheng/Common.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QPixmap>
#include <deque>

#include "../daheng/AcquisitionThread.h"




class DahengVideoDevice : VideoDevice
{
	Q_OBJECT

private:
	int index;

public:
	DahengVideoDevice(int n);

	CAcquisitionThread  *m_pobjAcqThread;           ///< Child image acquisition and process thread
	
	
    GX_DEV_HANDLE        m_hDevice;                 ///< Device Handle
    

    bool                 m_bAcquisitionStart;       ///< Flag : camera is acquiring or not

	bool m_bOpen = false; 

	int open();
	void close();

	bool isOpen();
	void loadFrame();


};

class DahengVideoDeviceManager : public VideoDeviceManager
{
private:
	QList<VideoDevice *> devices;

public:
	DahengVideoDeviceManager();

	GX_DEVICE_BASE_INFO *m_pstBaseInfo;             ///< Pointer struct of Device info

	QList<VideoDevice *> enumerate();

	uint32_t             m_ui32DeviceNum;           ///< Device number enumerated
};