/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "ui_settings.h"
#include "ui_about.h"

#include <videodevice.h>

#include <QSettings>
#include <QTimer>

class About : public QDialog
{
	Q_OBJECT

public:
	explicit About(QWidget *parent = nullptr);

private:
	Ui::About ui;
};

class Settings : public QDialog
{
	Q_OBJECT

public:
	explicit Settings(QWidget *parent = nullptr);
	~Settings();

private slots:
	/* void refresh(); */

private:
	Ui::Settings ui;
	QSettings settings;

	/* video stream input devices */
	VideoDevice *vdev_left = NULL, *vdev_right = NULL;

	/* calibration */
	void load();

protected:
	virtual void resizeEvent(QResizeEvent *e) override;
};
