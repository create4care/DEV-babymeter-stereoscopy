/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <imageview.h>
#include <opencv.h>

#include <QList>
#include <QLabel>
#include <QTimer>

/* default global refreshrate in ms */
#define VIDEODEVICE_REFRESHRATE 20

/* refreshrate timer */
static QTimer refreshtimer;

class VideoDevice : public QObject
{
	Q_OBJECT
private:
	QMap<void *, QMetaObject::Connection> connections;

protected:
	/* last frame pulled from the device */
	Mat image;

public:
	/*
	 * TODO implement uuid
	 * The idea of the UUID is that VideoDevices are identifiable after a
	 * re-enumeration. Unfortunately, I haven't found a way yet to retrieve
	 * a device-bound value using the VideoCapture framework
	 */

	/*
	 * Unique identifier to remember the VideoDev by internally.
	 * It is implementation defined how this is calculated (it must be
	 * consistent though).
	 * */
	/* QUuid uuid; */

	/* video device name (shown in the GUI) */
	QString name;

	/*
	 * Open the video device
	 * returns 1 on success, 0 or lower on failure
	 */
	virtual int open() = 0;

	/* close the video device */
	virtual void close()
	{
		if (!connections.empty())
			foreach (QMetaObject::Connection conn, connections.values())
				disconnect(conn);
		refreshtimer.start(VIDEODEVICE_REFRESHRATE);
	}

	/* check if the video device is opened */
	virtual bool isOpen() = 0;

	/* load one frame into the image buffer */
	virtual void loadFrame() = 0;

	/* retrieve the last videoframe from the videostream */
	Mat getFrame()
	{
		return image;
	}

	/*
	 * Start the video stream and write the image to a surface
	 * `surface` is a QLabel or ImageView to write to
	 * if `single` only one frame will be written to `surface`
	 * */
	template <typename ImageSurface>
	void stream(ImageSurface *surface, bool scale, bool single)
	{
		if (!isOpen())
			return;

		if (single) {
			loadFrame();
			if (image.empty())
				return;

			if (scale)
				surface->setPixmap(mattopixmap(image).scaled(surface->size(),
						Qt::KeepAspectRatio));
			else
				surface->setPixmap(mattopixmap(image));

			return;
		}

		/* TODO check if not already streaming to this surface */

		connections.insert(surface, connect(&refreshtimer, &QTimer::timeout,
				this, [this, surface, scale] {
			loadFrame();
			updateSurface(surface, scale);
			return;
			if (image.empty())
				return;

			if (scale)
				surface->setPixmap(mattopixmap(image).scaled(surface->size(),
						Qt::KeepAspectRatio));
			else
				surface->setPixmap(mattopixmap(image));
		}));
		refreshtimer.start(VIDEODEVICE_REFRESHRATE);
	}

	/* stop the active video stream */
	template <typename ImageSurface>
	void stop(ImageSurface *surface)
	{
		disconnect(connections.take(surface));
	}

	/* update the surface that this device is streaming to */
	template <typename ImageSurface>
	void updateSurface(ImageSurface *surface, bool scale)
	{
		if (image.empty())
			return;

		if (scale)
			surface->setPixmap(mattopixmap(image).scaled(surface->size(),
					Qt::KeepAspectRatio));
		else
			surface->setPixmap(mattopixmap(image));
	}
};

class VideoDeviceManager
{
public:
	virtual QList<VideoDevice *> enumerate() = 0;
};

/*
 * this is kind of a HACK (read: limitation of the VideoDevice framework) to
 * allow loading in images
 */
class StaticVideoDevice : VideoDevice
{
	Q_OBJECT

public:
	StaticVideoDevice() {}

	int open() { return 1; }
	void close() {}

	void load(Mat &source) {
		image = source;
	}

	bool isOpen() { return true; }
	void loadFrame() {}
};

QList<VideoDevice *> videodevices();
bool opencams(VideoDevice *&leftvdev, VideoDevice *&rightvdev);
void loadimgs(VideoDevice *&leftvdev, Mat &leftsrc, VideoDevice *&rightvdev, Mat &rightsrc);
