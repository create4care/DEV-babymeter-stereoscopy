/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <QPixmap>
#include <QSettings>
#include <QString>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

#define REFRESH_TIMEOUT 100 /* TODO remove */

QPixmap mattopixmap(Mat const &src);

QString mattostring(Mat src);
Mat stringtomat(QString _src);

bool opencam(VideoCapture &cap, QSettings &settings, const QString key);

int corners_find(Mat &img, std::vector<Point2f> &points, int w, int h);
void corners_draw(Mat &img, std::vector<Point2f> &points, int w, int h);

std::vector<Point3f> getobj(int w, int h, int n);

double calcrotation(Mat src);
