/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 David Kakes <davidkakes@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QPainter>

class Dot : public QGraphicsEllipseItem
{
public:
	Dot(qreal x, qreal y, qreal width, qreal height,
			QGraphicsItem *parent = nullptr);

	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem
			*option, QWidget *widget = nullptr) override;
};

class ImageView : public QGraphicsView
{
	Q_OBJECT

public:
	ImageView(QWidget *parent = 0);

	void setPixmap(const QPixmap &pixmap);

	void setCoordinates(QVector<QPointF> &pnts);
	QVector<QPointF> getCoordinates();
	void setSelEnabled(bool enable);
	void clearPoints();
	void undoLastPoint();

private:
	const double DOT_RADIUS = 1;	/* radius of the dot in px*/
	const double HITBOX_RADIUS = 10;	/* radius of the dot hitbox in px */
	const double LINE_WIDTH = 1;
	const double TEXT_SIZE = 5;
	const double MAGNIF_SCALE = 10;/* radius in scale of the scene */
	const double MAGNIF_ZOOM = 1.2;
	const double MAGNIF_PLACEMENT = 12;
	const double TEXT_PLACEMENT = 8; // HIGHER IS HIGHER ON THE IMAGE

	QGraphicsView *magnif;
	QGraphicsScene scene_main, scene_magnif;
	QGraphicsPixmapItem *pix_main = nullptr, *pix_magnif = nullptr;
	QGraphicsLineItem *vline, *hline;

	QList<Dot *> dots;
	QList<QGraphicsLineItem *> lines;
	bool selenabled = false;

	inline Dot *getSelectedDot();
	inline Dot *getHitbox(Dot *dot);
	void addPoint(QPointF &point, const double radius);
	inline QPointF getPoint(QGraphicsItem *dot);
	QGraphicsTextItem *getText(QGraphicsItem *dot);
	void moveMagnif(Dot *dot);

	void preventCollision(Dot *dot1, Dot *dot2);
	QLineF calcLine(QGraphicsItem *item1, QGraphicsItem *item2);

protected:
	virtual void mousePressEvent(QMouseEvent *ev) override;
	virtual void mouseReleaseEvent(QMouseEvent *ev) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *ev) override;
	virtual void mouseMoveEvent(QMouseEvent *ev) override;

signals:
	void mousePressed();
};
