/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <main.h>

#include <restore.h>
#include <segmentitemdelegate.h>
#include <settings.h>

#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QFontDatabase>
#include <QGraphicsColorizeEffect>
#include <QMessageBox>
#include <QShortcut>
#include <QWindow>
#ifdef DEBUG
#  include <QDebug>
#else
#  include <QGraphicsOpacityEffect>
#  include <QPropertyAnimation>
#endif

#include <opencv2/videoio/registry.hpp>

Main::Main(QWidget *parent) : QWidget(parent)
{
	ui.setupUi(this);

	/* load in SVGs */
	ui.baby->setPixmap(QIcon(":/res/baby.svg").pixmap(QSize(336, 124)));
	ui.logo->setPixmap(QIcon(":/res/logo.svg").pixmap(QSize(140, 140)));

	/* change color of logo to white */
	QGraphicsColorizeEffect *eff = new QGraphicsColorizeEffect(this);
	eff->setColor(Qt::white);
	eff->setStrength(1);
	ui.logo->setGraphicsEffect(eff);

	/* color SVG icons on the buttons on press/release */
	connect(ui.live, &QAbstractButton::pressed, this,
			[=] { button_press(ui.live); });
	connect(ui.live, &QAbstractButton::released, this,
			[=] { button_release(ui.live); });
	button_release(ui.live);

	connect(ui.save, &QAbstractButton::pressed, this,
			[=] { button_press(ui.save); });
	connect(ui.save, &QAbstractButton::released, this,
			[=] { button_release(ui.save); });
	button_release(ui.save);

	connect(ui.settings, &QAbstractButton::pressed, this,
			[=] { button_press(ui.settings); });
	connect(ui.settings, &QAbstractButton::released, this,
			[=] { button_release(ui.settings); });
	button_release(ui.settings);

	connect(ui.restore, &QAbstractButton::pressed, this,
			[=] { button_press(ui.restore); });
	connect(ui.restore, &QAbstractButton::released, this,
			[=] { button_release(ui.restore); });
	button_release(ui.restore);

	connect(ui.capture, &QAbstractButton::pressed, this,
			[=] { button_press(ui.capture); });
	connect(ui.capture, &QAbstractButton::released, this,
			[=] { button_release(ui.capture); });
	button_release(ui.capture);

#ifndef DEBUG
	/* show the splash screen */
	QLabel *splash = new QLabel(this);
	splash->setStyleSheet("QLabel { background-color: " UI_COLOR "; }");
	splash->setPixmap(QIcon(":/res/splash.svg").pixmap(QSize(360, 360)));
	splash->setFixedSize(this->size());
	splash->setAlignment(Qt::AlignCenter);
#endif

	/* shortcuts */
	QShortcut *fullscr = new QShortcut(QKeySequence::FullScreen, this);
	connect(fullscr, &QShortcut::activated, this, [=] {
		setWindowState(windowState() ^ Qt::WindowFullScreen);
	});

	QShortcut *undo = new QShortcut(QKeySequence::Undo, this);
	connect(undo, &QShortcut::activated, this, &Main::undolastpoint);
	QShortcut *clear = new QShortcut(QKeySequence::Delete, this);
	connect(clear, &QShortcut::activated, this, [=] {
		clearpoints();
		calculate(false);
	});

	/* ui elements */
	connect(ui.settings, &QAbstractButton::clicked, this, [=] {
		/* stop the camera streams */
		if (ui.controls->currentIndex() == CAPTURE)
			stop();

		Settings *settingswin = new Settings(this);
		settingswin->exec();
		delete settingswin;

		/* restart the camera streams */
		if (ui.controls->currentIndex() == CAPTURE)
			live();

		/*
		 * FIXME
		 * restart the capture stream if saving has been turned on,
		 * because otherwise only the points will be saved
		 */
	});
	connect(ui.restore, &QAbstractButton::clicked, this, [=] {
		Restore *restorewin = new Restore(this);
		/* stop the camera streams */
		if (ui.controls->currentIndex() == CAPTURE)
			stop();

		if (restorewin->exec() == QDialog::Accepted &&
				!restorewin->session.isEmpty())
			load_session(restorewin->session);
		else
			live();

		delete restorewin;
	});
	connect(ui.save, &QAbstractButton::clicked, this, [=] {
		QVector<QPointF> pointsleft = ui.left->getCoordinates();
		QVector<QPointF> pointsright = ui.right->getCoordinates();

		save_capture();
		/*
		 * FIXME
		 * unneccesary call to calculate(), need to redesign this into a
		 * whole separate class perhaps
		 */
		calculate(true);

		ui.save->setText((tr("    Saved")));
		ui.save->setEnabled(false);
		button_press(ui.save);
	});

	connect(ui.left, &ImageView::mousePressed, this, [=] {
		calculate(false);
	});
	connect(ui.right, &ImageView::mousePressed, this, [=] {
		calculate(false);
	});

	connect(ui.capture, &QAbstractButton::clicked, this, &Main::capture);
	connect(ui.live, &QAbstractButton::clicked, this, &Main::live);

	QSizePolicy livesp = ui.live->sizePolicy();
	livesp.setRetainSizeWhenHidden(true);
	ui.live->setSizePolicy(livesp);
	ui.live->setVisible(false);
	ui.restore->setSizePolicy(livesp);

	live();

#ifndef DEBUG
	/* fade out the splash screen */
	QTimer *splashtimer = new QTimer(this);
	splashtimer->setSingleShot(true);
	connect(splashtimer, &QTimer::timeout, this, [=] {
		QGraphicsOpacityEffect *eff = new QGraphicsOpacityEffect(this);
		splash->setGraphicsEffect(eff);
		QPropertyAnimation *a = new QPropertyAnimation(eff, "opacity");
		a->setDuration(2000);
		a->setStartValue(1);
		a->setEndValue(0);
		a->setEasingCurve(QEasingCurve::OutBack);
		a->start(QPropertyAnimation::DeleteWhenStopped);
		connect(a, SIGNAL(finished()), splash ,SLOT(hide()));
	});
	splashtimer->start(500);
#endif
}

/* color toolbutton PRESSED_COLOR on press */
void Main::button_press(QToolButton *btn)
{
	QGraphicsColorizeEffect *eff = new QGraphicsColorizeEffect(this);

	eff->setColor(PRESSED_COLOR);
	eff->setStrength(1);

	btn->setGraphicsEffect(eff);
}

/* color toolbutton NORMAL_COLOR on release */
void Main::button_release(QToolButton *btn)
{
	QGraphicsColorizeEffect *eff = new QGraphicsColorizeEffect(this);

	eff->setColor(NORMAL_COLOR);
	eff->setStrength(1);

	btn->setGraphicsEffect(eff);
}

/* start the video capturing process */
void Main::live()
{
	clearpoints();

	setWindowTitle(tr("Live - Babymeter 3"));

	/* setup controls */
	ui.controls->setCurrentIndex(CAPTURE);
	ui.live->setVisible(false);
	ui.save->setVisible(false);

	ui.left->setSelEnabled(false);
	ui.right->setSelEnabled(false);

	/* attempt to open the video devices */
	if (!opencams(vdev_left, vdev_right)) {
		stop();

		ui.left->setVisible(false);
		ui.right->setVisible(false);

		ui.save->setVisible(false);

		ui.capture->setVisible(false);
		ui.capture_instruction->setText(
				tr("Please select cameras in settings to capture"));

		return;
	}

	/* check if calibration has been performed */
	settings.beginGroup("calibration");
	if (!settings.contains("camera_left") ||
			!settings.contains("camera_right") ||
			!settings.contains("distortion_left") ||
			!settings.contains("distortion_right") ||
			!settings.contains("rotation_left") ||
			!settings.contains("rotation_right") ||
			!settings.contains("projection_left") ||
			!settings.contains("projection_right")) {
		settings.endGroup();

		stop();

		ui.left->setVisible(false);
		ui.right->setVisible(false);

		ui.save->setVisible(false);
		ui.restore->setVisible(false);

		ui.capture->setVisible(false);
		ui.capture_instruction->setText(tr("Please perform calibration"));
		repaint();

		return;
	}
	settings.endGroup();

	ui.left->setVisible(true);
	ui.right->setVisible(true);

	/* enable session restoring button if enabled */
	ui.restore->setVisible(settings.contains("save/path") &&
			!settings.value("save/path").toString().isEmpty());
	ui.save->setText((tr("    Save")));
	ui.save->setEnabled(true);
	button_release(ui.save);

	ui.capture->setVisible(true);
	ui.capture_instruction->setText(tr("Bring the baby in picture\n"
			"and press \"Capture\""));

	vdev_left->stream(ui.left, false, false);
	vdev_right->stream(ui.right, false, false);
}

/* close the camera streams if active */
void Main::stop()
{
	if (vdev_left)
		vdev_left->close();
	if (vdev_right)
		vdev_right->close();
}

/* load a capture from the saving directory */
void Main::load_session(QString timestamp)
{
	QDir dir(settings.value("save/path").toString());

	/* return if invalid path */
	if (!dir.exists())
		return;

	/* set time of capture as window title */
	lastcapture = timestamp;
	setWindowTitle(lastcapture + tr(" - Babymeter 3"));

	/* load images */
	Mat left = imread(dir.path().toStdString() + "/" +
			timestamp.toStdString() + "/left.jpg", IMREAD_COLOR);
	Mat right = imread(dir.path().toStdString() + "/" +
			timestamp.toStdString() + "/right.jpg", IMREAD_COLOR);
	loadimgs(vdev_left, left, vdev_right, right);

	ui.left->setVisible(true);
	ui.left->setSelEnabled(true);

	ui.right->setVisible(true);
	ui.right->setSelEnabled(true);

	vdev_left->stream(ui.left, false, true);
	vdev_right->stream(ui.right, false, true);

	/* load points */
	QSettings pointsfile(dir.path() + "/" + timestamp + "/points.ini",
			QSettings::IniFormat);

	QVector<QPointF> pointsleft;
	int lsize = pointsfile.beginReadArray("left");
	for (int i = 0; i < lsize; i++) {
		pointsfile.setArrayIndex(i);
		pointsleft.append(QPointF(pointsfile.value("x").toReal(),
					pointsfile.value("y").toReal()));
	}
	pointsfile.endArray();
	ui.left->setCoordinates(pointsleft);

	QVector<QPointF> pointsright;
	int rsize = pointsfile.beginReadArray("right");
	for (int i = 0; i < rsize; i++) {
		pointsfile.setArrayIndex(i);
		pointsright.append(QPointF(pointsfile.value("x").toReal(),
					pointsfile.value("y").toReal()));
	}
	pointsfile.endArray();
	ui.right->setCoordinates(pointsright);

	ui.live->setVisible(true);
	if (settings.contains("save/auto") &&
			settings.value("save/auto").toBool())
		ui.save->setVisible(false);
	else
		ui.save->setVisible(true);

	calculate(false);

	ui.save->setText((tr("    Saved")));
	ui.save->setEnabled(false);
	button_press(ui.save);
}

/* save the capture to saving directory if enabled */
void Main::save_capture()
{
	if (!settings.contains("save/path") ||
			settings.value("save/path").toString().isEmpty()) {
		ui.save->setVisible(false);
		ui.restore->setVisible(false);
		return;
	}
	QDir dir(settings.value("save/path").toString());

	/* return and set empty path if path is invalid */
	if (!dir.exists()) {
		settings.setValue("save/path", "");
		ui.save->setVisible(false);
		ui.restore->setVisible(false);
		return;
	}

	/* create a new dir with the current timestamp */
	dir.mkdir(lastcapture);

	std::vector<int> param;
	param.push_back(IMWRITE_JPEG_QUALITY);
	param.push_back(95);

	imwrite(dir.path().toStdString() + "/" + lastcapture.toStdString() +
			"/left.jpg", vdev_left->getFrame(), param);
	imwrite(dir.path().toStdString() + "/" + lastcapture.toStdString() +
			"/right.jpg", vdev_right->getFrame(), param);
}

void Main::capture()
{
	stop();

	/* set time of capture as window title */
	lastcapture =
		QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss");
	setWindowTitle(lastcapture + tr(" - Babymeter 3"));

	ui.controls->setCurrentIndex(SELECT);
	ui.live->setVisible(true);

	ui.baby->setVisible(true);
	ui.select_instruction->setText(tr("Mark body points<br/>"
			"on both images</p><p>Drag to adjust</p>"));

	/* save capture if autosave is enabled, enabled save button otherwise */
	if (settings.contains("save/path") &&
			!settings.value("save/path").toString().isEmpty()) {
		if (settings.contains("save/auto") &&
				settings.value("save/auto").toBool()) {
			save_capture();
			ui.save->setVisible(false);
		} else {
			ui.save->setVisible(true);
		}
	} else {
		ui.save->setVisible(false);
	}

	ui.left->setSelEnabled(true);
	ui.right->setSelEnabled(true);

	repaint();
}

/* undo the last point set */
void Main::undolastpoint()
{
	int left = ui.left->getCoordinates().size();
	int right = ui.right->getCoordinates().size();

	if (left >= right)
		ui.left->undoLastPoint();
	if (right >= left)
		ui.right->undoLastPoint();

	calculate(false);
}

/* clear all points from both pixmaps */
void Main::clearpoints()
{
	ui.left->clearPoints();
	ui.right->clearPoints();
}

/* resize pixmaps on window resize */
void Main::resizeEvent(QResizeEvent *e)
{
	(void) e;

	if (vdev_left)
		vdev_left->updateSurface(ui.left, false);
	if (vdev_right)
		vdev_right->updateSurface(ui.right, false);
}

/* save image coordinates of left and right points to an INI file */
void Main::save_points(QVector<QPointF> &left, QVector<QPointF> &right)
{
	if (!settings.contains("save/path") ||
			settings.value("save/path").toString().isEmpty())
		return;
	QDir dir(settings.value("save/path").toString());

	/* return if invalid path */
	if (!dir.exists())
		return;

	QSettings file(dir.path() + "/" + lastcapture + "/points.ini",
			QSettings::IniFormat);

	file.remove("left");
	file.beginWriteArray("left");
	for (int i = 0; i < left.size(); i++) {
		file.setArrayIndex(i);
		file.setValue("x", left[i].x());
		file.setValue("y", left[i].y());
	}
	file.endArray();

	file.remove("right");
	file.beginWriteArray("right");
	for (int i = 0; i < right.size(); i++) {
		file.setArrayIndex(i);
		file.setValue("x", right[i].x());
		file.setValue("y", right[i].y());
	}
	file.endArray();
}

/* save segment lengths and total length to a CSV file */
void Main::save_results(QVector<float> &segments, std::vector<Point2f> &left,
		std::vector<Point2f> &right)
{
	if (!settings.contains("save/path") ||
			settings.value("save/path").toString().isEmpty())
		return;
	QDir dir(settings.value("save/path").toString());

	/* return if invalid path */
	if (!dir.exists())
		return;

	QFile file(dir.path() + "/" + lastcapture + "/results.csv");

	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;

	QTextStream str(&file);

	/* write headers */
	str << "x_left,y_left,x_right,y_right,distance_previous_point_cm\n";

	/* first entry doesn't have a distance to previous point (last col) */
	str << QString::number(left[0].x, 'f', 2) << ","
		<< QString::number(left[0].y, 'f', 2) << ","
		<< QString::number(right[0].x, 'f', 2) << ","
		<< QString::number(right[0].y, 'f', 2) << "\n";

	for (unsigned long i = 1; i < left.size() && i < right.size(); i++) {
		str << QString::number(left[i].x, 'f', 2) << ","
			<< QString::number(left[i].y, 'f', 2) << ","
			<< QString::number(right[i].x, 'f', 2) << ","
			<< QString::number(right[i].y, 'f', 2) << ","
			<< QString::number(segments[i - 1] / 10, 'f', 2) << "\n";
	}

	file.close();
}

/* FIXME what an absolute mess this function is */
void Main::calculate(bool save)
{
	QVector<QPointF> pointsleft = ui.left->getCoordinates();
	QVector<QPointF> pointsright = ui.right->getCoordinates();
	Mat camleft, camright;	/* camera matrices */
	Mat disleft, disright;	/* distortion coefficients */
	Mat rotleft, rotright;	/* rotation matrices */
	Mat proleft, proright;	/* projection matrices */

	/* save points to a file */
	if (save || (settings.contains("save/auto") &&
			settings.value("save/auto").toBool()))
		save_points(pointsleft, pointsright);

	/* enable save button if it was disabled before */
	ui.save->setText((tr("    Save")));
	ui.save->setEnabled(true);
	button_release(ui.save);

	if (pointsleft.size() < 2 || pointsright.size() < 2) {
		ui.controls->setCurrentIndex(SELECT);

		/* TODO delete results file if it exists */

		return;
	}

	ui.controls->setCurrentIndex(REVIEW);

	/* load settings */
	settings.beginGroup("calibration");
	camleft = stringtomat(settings.value("camera_left").toString());
	camright = stringtomat(settings.value("camera_right").toString());
	disleft = stringtomat(settings.value("distortion_left").toString());
	disright = stringtomat(settings.value("distortion_right").toString());
	rotleft = stringtomat(settings.value("rotation_left").toString());
	rotright = stringtomat(settings.value("rotation_right").toString());
	proleft = stringtomat(settings.value("projection_left").toString());
	proright = stringtomat(settings.value("projection_right").toString());
	settings.endGroup();

	std::vector<Point2f> coordsleft, coordsright;
	Mat ucoordsleft, ucoordsright;
	Mat coords4d, coords3d;

	/* scale the pixmap points to the images' pixel coordinates */
	for (int i = 0; i < pointsleft.size() && i < pointsright.size(); i++) {
		Point2f pnt;

		pnt.x = (double) vdev_left->getFrame().size().width /
			ui.left->scene()->width() * pointsleft[i].x();
		pnt.y = (double) vdev_left->getFrame().size().height /
			ui.left->scene()->height() * pointsleft[i].y();
		coordsleft.push_back(pnt);

		pnt.x = (double) vdev_right->getFrame().size().width /
			ui.right->scene()->width() * pointsright[i].x();
		pnt.y = (double) vdev_right->getFrame().size().height /
			ui.right->scene()->height() * pointsright[i].y();
		coordsright.push_back(pnt);
	}

	/* undistort and triangulate */
	undistortPoints(coordsleft, ucoordsleft, camleft, disleft, rotleft,
			proleft);
	undistortPoints(coordsright, ucoordsright, camright, disright, rotright,
			proright);

	triangulatePoints(proleft, proright, ucoordsleft, ucoordsright,
			coords4d);
	transpose(coords4d, coords3d);
	convertPointsFromHomogeneous(coords3d, coords3d);

	/* calculate the segment points */
	QVector<float> segments;

	QStandardItemModel *model = new QStandardItemModel(this);
	model->setColumnCount(2);
	for (int i = 0; i < pointsleft.size() - 1 && i < pointsright.size() - 1;
			i++) {
		float dis = hypot(hypot(coords3d.at<float>(i, 0) -
					coords3d.at<float>(i + 1, 0),
					coords3d.at<float>(i, 1) -
					coords3d.at<float>(i + 1, 1)),
					coords3d.at<float>(i, 2) -
					coords3d.at<float>(i + 1, 2));

		/* c++17 hypot (not implemented in clang?)*/
		/* dis = hypot(hypot(coords1_l[0] - coords1_r[0], coords1_l[1] -
					coords1_r[1]), coords1_l[2] -
					coords1_r[2]); */

		segments.push_back(dis);

		QList<QStandardItem *> row;
		QStandardItem *point = new QStandardItem();
		point->setText("Segment " + QString::number(i + 1) + " - " +
				QString::number(i + 2));
		row.append(point);

		QStandardItem *length = new QStandardItem();
		length->setText(QString::number(dis / 10, 'f', 2) + " cm");
		length->setTextAlignment(Qt::AlignRight);
		row.append(length);

		model->appendRow(row);
	}

	/* calculate the total length */
	float total = 0.0;

	for (auto &n : segments)
		total += n;

	/* update the ui */
	ui.length->setText(tr("<div style='font-size:14px'>Total length</div>"
			"<div style='font-size: 24px;'>") +
			QString::number(total / 10, 'f', 2) + " cm</div>");
	ui.segments->setModel(model);
	ui.segments->setItemDelegate(new SegmentItemDelegate());

	/* save results to a file */
	if (save || (settings.contains("save/auto") &&
			settings.value("save/auto").toBool()))
		save_results(segments, coordsleft, coordsright);

	repaint();
}

int main(int argc, char *argv[])
{
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication app(argc, argv);

	app.setOrganizationName("create4care");
	app.setApplicationName("babymeter3");
	app.setApplicationDisplayName("Babymeter 3");

	/* load custom fonts */
	QFontDatabase::addApplicationFont(":/fonts/OpenSans-Regular.ttf");
	QFontDatabase::addApplicationFont(":/fonts/OpenSans-Bold.ttf");

	/* register custom types */
	qRegisterMetaType<QVector<int>>();

#ifdef DEBUG
	qDebug() << "supported OpenCV VideoCapture backends:";
	for (auto &api : videoio_registry::getBackends())
		qDebug() << QString::fromStdString(
				videoio_registry::getBackendName(api));

	qDebug() << "discovered video devices:";
	for (auto &videodev : videodevices())
		qDebug() << videodev->name;
#endif

	Main *ui = new Main;

	ui->show();

	ui->windowHandle()->setIcon(QIcon(":/res/icon.svg"));

	return app.exec();
}
