QT	       += gui widgets svg
QTPLUGIN       += qtvirtualkeyboardplugin
INCLUDEPATH    += include/
UI_DIR		= include/

CONFIG	       += object_parallel_to_source

RESOURCES	= gui/resources.qrc
FORMS	       += \
		  gui/calibsetup.ui \
		  gui/calibcapture.ui \
		  gui/calibreview.ui \
		  gui/about.ui \
		  gui/settings.ui \
		  gui/restore.ui \
		  gui/main.ui
HEADERS	       += \
		  include/videobackend/opencv.h \
		  include/videobackend/daheng.h \
		  include/videodevice.h \
		  include/opencv.h \
		  include/imageview.h \
		  include/segmentitemdelegate.h \
		  include/calib.h \
		  include/settings.h \
		  include/restore.h \
		  include/main.h \
		  include/daheng/DxImageProc.h \
		  include/daheng/GxIAPI.h
SOURCES	       += \
		  videobackend/opencv.cpp \
		  videobackend/daheng.cpp \
		  videodevice.cpp \
		  opencv.cpp \
		  imageview.cpp \
		  segmentitemdelegate.cpp \
		  calib.cpp \
		  settings.cpp \
		  restore.cpp \
		  main.cpp

babymeter.files	= babymeter
babymeter.path	= /usr/local/bin/
INSTALLS       += babymeter

debug {
	DEFINES	       += DEBUG
}

unix {
	CONFIG	       += link_pkgconfig
	PKGCONFIG      += opencv4

	target.path	= /usr/local/bin/
	target.from	= babymeter
	INSTALLS       += target
}

macx {
	debug {
		CONFIG	       -= app_bundle
	}
	QMAKE_CXXFLAGS += -std=c++11
}

win32 {
	QTPLUGIN	      -= qtvirtualkeyboardplugin

	debug {
		CONFIG	      += console
	}

	static {
		CONFIG	      += static
		CONFIG	      -= debug_and_release

		LIBS	      += \
				 opencv/win32/lib/libopencv_highgui420.a \
				 opencv/win32/lib/libopencv_videoio420.a \
				 opencv/win32/lib/libopencv_calib3d420.a \
				 opencv/win32/lib/libopencv_imgcodecs420.a \
				 opencv/win32/lib/libopencv_flann420.a \
				 opencv/win32/lib/libopencv_imgproc420.a \
				 opencv/win32/lib/libopencv_core420.a \
				 opencv/win32/lib/liblibjpeg-turbo.a \
				 opencv/win32/lib/liblibpng.a
		INCLUDEPATH   += opencv/win32/include
	}
}
