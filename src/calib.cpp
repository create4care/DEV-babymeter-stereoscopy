/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <calib.h>

#include <QApplication>
#include <QDateTime>
#include <QFileDialog>
#include <QMessageBox>

bool calibrated;

/* images buffer */
static std::vector<Mat> imgs_left, imgs_right;
/* points buffer */
static std::vector<std::vector<Point2f>> pnts_left, pnts_right;

CalibWizard::CalibWizard(QWidget *parent) : QWizard(parent)
{
	this->setWindowFlags(this->windowFlags() &
			~Qt::WindowContextHelpButtonHint);

	setPage(Setup, new CalibSetup);
	setPage(Capture, new CalibCapture);
	setPage(Review, new CalibReview);

	setWindowTitle(tr("Calibration - Babymeter 3"));
	setMinimumSize(640, 522);
	setWizardStyle(QWizard::NStyles);
	setOptions(0);
	setWindowModality(Qt::WindowModal);

	/* clear points from previous calibration during same session */
	imgs_left.clear();
	pnts_left.clear();
	imgs_right.clear();
	pnts_right.clear();
	calibrated = false;
}

/* ask the user to discard all changes and exit calibration */
int CalibWizard::close_confirm()
{
	QMessageBox *confirmclose = new QMessageBox(this);

	confirmclose->setText(tr("Are you sure you want to exit calibration?"));
	confirmclose->setInformativeText(tr("All progress will be discarded."));
	confirmclose->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	confirmclose->setDefaultButton(QMessageBox::No);
	confirmclose->setWindowModality(Qt::WindowModal);
	confirmclose->setWindowFlags(confirmclose->windowFlags() &
			~Qt::WindowContextHelpButtonHint);

	confirmclose->deleteLater();

	return confirmclose->exec();
}

void CalibWizard::closeEvent(QCloseEvent *e)
{
	if (calibrated || close_confirm() == QMessageBox::Yes)
		e->accept();
	else
		e->ignore();
}

void CalibWizard::reject()
{
	if (close_confirm() == QMessageBox::Yes)
		QWizard::reject();
}

/*
 * Setup Page
 */
CalibSetup::CalibSetup(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);

	connect(ui.autocap_interval, &QAbstractSlider::valueChanged, this,
			&CalibSetup::updateinterval);

	connect(ui.source_file, &QAbstractButton::toggled, this,
			&CalibSetup::completeChanged);

	connect(ui.left_path, &QLineEdit::textChanged, this,
			&CalibSetup::completeChanged);
	connect(ui.left_browse, &QAbstractButton::clicked, this, [=] {
		QFileDialog fd(this);
		fd.setWindowTitle(tr("Open left samples directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (fd.exec())
			ui.left_path->setText(fd.selectedFiles()[0]);
	});

	connect(ui.right_path, &QLineEdit::textChanged, this,
			&CalibSetup::completeChanged);
	connect(ui.right_browse, &QAbstractButton::clicked, this, [=] {
		QFileDialog fd(this);
		fd.setWindowTitle(tr("Open right samples directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (fd.exec())
			ui.right_path->setText(fd.selectedFiles()[0]);
	});

	connect(&watchimgloading, &QFutureWatcher<void>::finished, this, [=] {
		/* restore controls */
		ui.dimencontrols->setEnabled(true);
		ui.squaresizecontrols->setEnabled(true);
		ui.source_capture->setEnabled(true);
		ui.source_file->setEnabled(true);
		ui.left->setEnabled(true);
		ui.right->setEnabled(true);
		ui.cancel->setEnabled(true);
		ui.status->setVisible(false);

		repaint();

		if (!cancelimgloading) {
			imgsloaded = true;
			wizard()->next();
		} else {
			cancelimgloading = false;
		}

		emit completeChanged();
	});

	/* file loading status text */
	ui.status->setVisible(false);
	connect(this, &CalibSetup::file_load, this, [=] (int i, int max) {
		ui.status_text->setText(tr("Loading sample ") +
				QString::number(i) + "/" + QString::number(max)
				+ tr("..."));
	});

	connect(ui.cancel, &QAbstractButton::clicked, this, [=] {
		ui.cancel->setEnabled(false);
		repaint();

		cancelimgloading = true;
	});

	registerField("boardwidth", ui.boardwidth);
	registerField("boardheight", ui.boardheight);
	registerField("squaresize", ui.squaresize);
	registerField("source", ui.source_capture);
	registerField("autocap", ui.autocap);
	registerField("autocap_interval", ui.autocap_interval);
	registerField("left_path", ui.left_path);
	registerField("right_path", ui.right_path);

	settings.beginGroup("camera");

	if (!settings.contains("left/index") || !settings.contains("right/index")) {
		ui.source_capture->setEnabled(false);
		ui.source_file->setChecked(true);
		ui.capturing->hide();
		ui.load->show();
	} else {
		ui.source_warning->setVisible(false);
		ui.load->hide();
	}

	settings.endGroup();
}

/* load images from files */
void CalibSetup::loadimgs(const QString &lpath, const QString &rpath)
{
	QDir leftdir(lpath);
	QDir rightdir(rpath);

	/* TODO set filter in qfiledialog */
	QStringList filesleft = leftdir.entryList(QStringList() << "*.jpg" <<
			"*.JPG" << "*.jpeg" << "*.JPG", QDir::Files);
	QStringList filesright = rightdir.entryList(QStringList() << "*.jpg" <<
			"*.JPG" << "*.jpeg" << "*.JPG", QDir::Files);

	/* TODO warn if not enough valid samples found */

	/* TODO check for same number of images and same file names */

	const int w = field("boardwidth").toInt();
	const int h = field("boardheight").toInt();

	for (int i = 0; i < filesleft.size(); i++) {
		std::vector<Point2f> pntsleft, pntsright;

		if (cancelimgloading) {
			imgs_left.clear();
			pnts_left.clear();

			imgs_right.clear();
			pnts_right.clear();

			return;
		}

		/* update the status display */
		emit file_load(i + 1, filesleft.size());

		Mat imgleft = imread(leftdir.path().toStdString() + "/" +
				filesleft.at(i).toStdString(), IMREAD_COLOR);
		Mat imgright = imread(rightdir.path().toStdString() + "/" +
				filesright.at(i).toStdString(), IMREAD_COLOR);

		if ((!imgleft.empty() && !corners_find(imgleft, pntsleft, w, h))
				|| (!imgright.empty() && !corners_find(imgright,
						pntsright, w, h)))
			continue;

		imgs_left.push_back(imgleft.clone());
		pnts_left.push_back(pntsleft);

		imgs_right.push_back(imgright.clone());
		pnts_right.push_back(pntsright);
	}
}

bool CalibSetup::validatePage()
{
	/* only execute the code below if file loading is needed */
	if (ui.source_capture->isChecked())
		return true;

	/* if samples have been loaded */
	if (imgsloaded) {
		imgsloaded = false;
		return true;
	}

	/* disable all controls */
	ui.dimencontrols->setEnabled(false);
	ui.squaresizecontrols->setEnabled(false);
	ui.source_capture->setEnabled(false);
	ui.source_file->setEnabled(false);
	ui.left->setEnabled(false);
	ui.right->setEnabled(false);

	/* load images in a new thread */
	imgloading = QtConcurrent::run(this, &CalibSetup::loadimgs,
			ui.left_path->text(), ui.right_path->text());
	ui.status->setVisible(true);
	watchimgloading.setFuture(imgloading);

	emit completeChanged();
	repaint();

	return false;
}

bool CalibSetup::isComplete() const
{
	if (ui.source_capture->isChecked())
		return true;

	/* if samples are currently being loaded */
	if (watchimgloading.isRunning())
		return false;

	/* check if paths exists */
	if (ui.left_path->text().isEmpty() || ui.right_path->text().isEmpty())
		return false;

	QDir leftdir(ui.left_path->text());
	QDir rightdir(ui.right_path->text());

	return leftdir.exists() && rightdir.exists();
}

/* go to the appropriate page depending on which sample source is selected */
int CalibSetup::nextId() const
{
	if (ui.source_capture->isChecked())
		return CalibWizard::Capture;
	else
		return CalibWizard::Review;
}

/* update autocapture interval slider label */
void CalibSetup::updateinterval(int num)
{
	ui.autocap_label2->setText(QString::number(num * AUTOCAP_MULTIPLIER) +
			" ms");
}

/*
 * Capture Page
 */
CalibCapture::CalibCapture(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);

	refreshtimer = new QTimer(this);
	connect(refreshtimer, &QTimer::timeout, this, &CalibCapture::refresh);

	capturetimer = new QTimer(this);
	capturetimer->setSingleShot(true);
	connect(capturetimer, &QTimer::timeout, this, &CalibCapture::live);

	autocaptimer = new QTimer(this);
	connect(autocaptimer, &QTimer::timeout, this,
			&CalibCapture::autocapture);

	connect(ui.capture, &QAbstractButton::clicked, this,
			&CalibCapture::capture);
}

/* start the videocapture streams */
void CalibCapture::initializePage()
{
	/* open the camera streams */
	if (opencam(cap_left, settings, "left"))
		rggb_left = settings.value("camera/left/rggb").toBool();
	if (opencam(cap_right, settings, "right"))
		rggb_right = settings.value("camera/right/rggb").toBool();

	live();
}

void CalibCapture::cleanupPage()
{
	refreshtimer->stop();
	autocaptimer->stop();
	capturetimer->stop();

	if (cap_left.isOpened())
		cap_left.release();
	live_left.release();
	imgs_left.clear();
	pnts_left.clear();

	if (cap_right.isOpened())
		cap_right.release();
	live_right.release();
	imgs_right.clear();
	pnts_right.clear();
}

bool CalibCapture::validatePage()
{
	refreshtimer->stop();
	autocaptimer->stop();
	capturetimer->stop();

	if (cap_left.isOpened())
		cap_left.release();
	live_left.release();

	if (cap_right.isOpened())
		cap_right.release();
	live_right.release();

	return true;
}

bool CalibCapture::isComplete() const
{
	return imgs_left.size() >= MINSAMPLES;
}

/* start the video capturing process */
void CalibCapture::live()
{
	refreshtimer->start(REFRESH_TIMEOUT);

	ui.warning->setVisible(false);

	ui.index->setText(QString::number(imgs_left.size() + 1) + "/" +
			QString::number(MINSAMPLES));

	/* return if 1+ camera is not available and disable capturing */
	if (!cap_left.isOpened() || !cap_right.isOpened()) {
		ui.countdown->setVisible(false);
		ui.capture->setEnabled(false);
		return;
	}

	/* setup auto capturing if selected by the user */
	if (field("autocap").toBool()) {
		ui.countdown->setVisible(true);
		ui.capture->setVisible(false);

		autocaptimeout = field("autocap_interval").toInt();
		ui.countdown->setNum(floor((double) autocaptimeout *
				AUTOCAP_MULTIPLIER / 1000));

		autocaptimer->start(AUTOCAP_MULTIPLIER);
	} else {
		ui.countdown->setVisible(false);
		ui.capture->setVisible(true);
	}
}

/* update the buffer with the videostream and refresh the pixmaps on screen */
void CalibCapture::refresh()
{
	if (cap_left.isOpened()) {
		cap_left >> live_left;
		if (rggb_left) {
			try {
				cvtColor(live_left, live_left,
						COLOR_BayerRG2RGB);
			} catch (Exception &e) { (void) e; }
		}
	}

	if (!live_left.empty()) {
		QPixmap pix = mattopixmap(live_left);

		ui.left->setPixmap(pix.scaled(ui.left->size(),
					Qt::KeepAspectRatio));
	}

	if (cap_right.isOpened()) {
		cap_right >> live_right;
		if (rggb_right) {
			try {
				cvtColor(live_right, live_right,
						COLOR_BayerRG2RGB);
			} catch (Exception &e) { (void) e; }
		}
	}

	if (!live_right.empty()) {
		QPixmap pix = mattopixmap(live_right);

		ui.right->setPixmap(pix.scaled(ui.right->size(),
					Qt::KeepAspectRatio));
	}

	repaint();
}

/* resize pixmaps on window resize */
void CalibCapture::resizeEvent(QResizeEvent *e)
{
	(void) e;

	refresh();
}

/* capture a sample */
void CalibCapture::capture()
{
	std::vector<Point2f> pntsleft, pntsright;
	QPixmap pixleft, pixright;
	const int w = field("boardwidth").toInt();
	const int h = field("boardheight").toInt();

	refreshtimer->stop();
	autocaptimer->stop();

	ui.capture->setVisible(false);
	ui.countdown->setVisible(false);

	/* show warning for 1 sec if corners can't be found in either sample */
	if ((!live_left.empty() && !corners_find(live_left, pntsleft, w, h)) ||
			(!live_right.empty() && !corners_find(live_right,
							      pntsright, w,
							      h))) {
		ui.warning->setVisible(true);
		repaint();
	} else {
		/* save the images to the buffer if the corners are detected */
		imgs_left.push_back(live_left.clone());
		pnts_left.push_back(pntsleft);

		imgs_right.push_back(live_right.clone());
		pnts_right.push_back(pntsright);

		/* show corners on screen */
		corners_draw(live_left, pntsleft, w, h);
		corners_draw(live_right, pntsright, w, h);

		pixleft = mattopixmap(live_left);
		ui.left->setPixmap(pixleft.scaled(ui.left->size(),
					Qt::KeepAspectRatio));

		pixright = mattopixmap(live_right);
		ui.right->setPixmap(pixright.scaled(ui.right->size(),
					Qt::KeepAspectRatio));

		repaint();
	}

	emit completeChanged();
	capturetimer->start(CAPTURE_TIMEOUT);
}

/* update autocaptimeout or capture if timeout has expired */
void CalibCapture::autocapture()
{
	double sec = (double) --autocaptimeout * AUTOCAP_MULTIPLIER / 1000;

	/* only update screen with whole seconds */
	if (floor(sec) == sec)
		ui.countdown->setNum(sec);
	/* else if (sec == (double) AUTOCAP_MULTIPLIER / 1000)
		ui.countdown->setNum(0); */

	/* capture when timeout has expired */
	if (!autocaptimeout)
		capture();
}

/*
 * Review Page
 */
CalibReview::CalibReview(QWidget *parent) : QWizardPage(parent)
{
	ui.setupUi(this);

	connect(ui.prev, &QAbstractButton::clicked, this, [=] {
		cycle(-1);
	});
	connect(ui.next, &QAbstractButton::clicked, this, [=] {
		cycle(1);
	});
	connect(ui.gridlines, &QCheckBox::stateChanged, this, [=] {
		cycle(0);
	});

	connect(ui.discard, &QAbstractButton::clicked, this, &CalibReview::discard);

	connect(ui.exportsample, &QAbstractButton::clicked, this, [=] {
		QMessageBox::information(this, QApplication::applicationDisplayName(),
				tr("Sorry this behavior is not yet implemented"));
	});
	connect(ui.exportall, &QAbstractButton::clicked, this, [=] {
		std::vector<int> param;
		param.push_back(IMWRITE_JPEG_QUALITY);
		param.push_back(95);

		QFileDialog fd(this);
		fd.setWindowTitle(tr("Export all to directory"));
		fd.setFileMode(QFileDialog::Directory);

		if (!fd.exec() || fd.selectedFiles()[0].isEmpty())
			return;

		QDir dir(fd.selectedFiles()[0]);

		if (!dir.mkdir(tr("left")) || !dir.mkdir(tr("right"))) {
			/* TODO handle error */
			return;
		}

		for (unsigned int i = 0; i < imgs_left.size(); i++) {
			imwrite(dir.path().toStdString() + "/left/" +
					QString::number(i).toStdString() +
					".jpg", imgs_left[i], param);
			imwrite(dir.path().toStdString() + "/right/" +
					QString::number(i).toStdString() +
					".jpg", imgs_right[i], param);
		}

		QMessageBox::information(this, "Babymeter 3", tr("Exported ") +
				QString::number(imgs_left.size()) + tr(" file(s)"));
	});

	connect(&watchcalibrating, &QFutureWatcher<void>::finished, this, [=] {
		calibrated = true;
		emit completeChanged();
		wizard()->close();
	});

	/* file loading status text */
	ui.status->setVisible(false);
	connect(this, &CalibReview::calibrate_status, this, [=] (QString str) {
		ui.status->setText(str);
	});
}

void CalibReview::initializePage()
{
	if (field("source").toBool()) {
		ui.label1->setVisible(true);
		ui.label2->setVisible(false);
		ui.exportsample->setVisible(true);
		ui.exportall->setVisible(true);
	} else {
		ui.label1->setVisible(false);
		ui.label2->setVisible(true);
		ui.discard->setText(tr("Exclude"));
		ui.exportsample->setVisible(false);
		ui.exportall->setVisible(false);
	}

	index = 0;
}

void CalibReview::showEvent(QShowEvent *ev)
{
	QWizardPage::showEvent(ev);
	cycle(0);
}

/* resize pixmaps on window resize */
void CalibReview::resizeEvent(QResizeEvent *e)
{
	(void) e;

	cycle(0);
}

void CalibReview::cleanupPage()
{
	if (field("source").toBool())
		return;

	imgs_left.clear();
	pnts_left.clear();

	imgs_right.clear();
	pnts_right.clear();
}

/* perform calibration, save changes and exit calibration */
void CalibReview::calibrate(const int boardwidth, const int boardheight,
		const int squaresize)
{
	const Size size = imgs_left[0].size();

	/*
	 * 1.
	 * assemble array of object points
	 */
	std::vector<std::vector<Point3f>> obj;

	for (unsigned int i = 0; i < imgs_left.size(); i++)
		obj.push_back(getobj(boardwidth, boardheight, squaresize));

	/*
	 * 2.
	 * calibrate left and right camera
	 */
	Mat camleft, camright;			/* camera matrices */
	Mat disleft, disright;			/* distortion coefficients */
	std::vector<Mat> rvecsleft, rvecsright;	/* rotation vectors */
	std::vector<Mat> tvecsleft, tvecsright;	/* translation vectors */

	emit calibrate_status(tr("Calibrating cameras..."));
	const int flags = CALIB_FIX_K4 | CALIB_FIX_K5;
	calibrateCamera(obj, pnts_left, size, camleft, disleft, rvecsleft,
			tvecsleft, flags);
	calibrateCamera(obj, pnts_right, size, camright, disright, rvecsright,
			tvecsright, flags);

	/*
	 * 3.
	 * calibrate the cameras for stereoscopy
	 */
	Mat rot;	/* rotation matrix */
	Vec3d trans;	/* translation vector */
	Mat ess;	/* essential matrix */
	Mat fun;	/* fundamental matrix */
	double error;	/* reprojection error */

	emit calibrate_status(tr("Calculating stereoscopic parameters..."));
	error = stereoCalibrate(obj, pnts_left, pnts_right, camleft, disleft,
			camright, disright, size, rot, trans, ess, fun);

	/*
	 * 4.
	 * compute the rectification transformations
	 */
	Mat rotleft, rotright;	/* rotation matrices */
	Mat proleft, proright;	/* projection matrices */
	Mat dtdmap;		/* disparity-to-depth mapping matrix */

	emit calibrate_status(tr("Rectifying stereoscopic parameters..."));
	stereoRectify(camleft, disleft, camright, disright, size, rot, trans,
			rotleft, rotright, proleft, proright, dtdmap);

	/* not needed right now, perhaps in the future */
#if 0
	/*
	 * 5.
	 * compute the undistortion and rectification transformation map
	 */
	Mat mapxleft, mapyleft;		/* left camera maps */
	Mat mapxright, mapyright;	/* right camera maps */

	initUndistortRectifyMap(camleft, disleft, rotleft, proleft, size,
			CV_32F, mapxleft, mapyleft);
	initUndistortRectifyMap(camright, disright, rotright, proright, size,
			CV_32F, mapxright, mapyright);
#endif

	/*
	 * save all chessboard and calibration data to this location:
	 * - Linux: ~/.config/babymeter/babymeter3.conf
	 * - macOS: [Application Home Directory]/Settings/babymeter/babymeter3
	 * - Windows: HKCU\Software\babymeter\babymeter3
	 */
	settings.beginGroup("calibration");
	settings.setValue("date", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
	settings.setValue("error", error);
	settings.setValue("squaresize", QString::number(squaresize));
	settings.setValue("boardwidth", QString::number(boardwidth));
	settings.setValue("boardheight", QString::number(boardheight));
	settings.setValue("camera_left", mattostring(camleft));
	settings.setValue("camera_right", mattostring(camright));
	settings.setValue("distortion_left", mattostring(disleft));
	settings.setValue("distortion_right", mattostring(disright));
	settings.setValue("rotation_left", mattostring(rotleft));
	settings.setValue("rotation_right", mattostring(rotright));
	settings.setValue("projection_left", mattostring(proleft));
	settings.setValue("projection_right", mattostring(proright));
	settings.setValue("rotation", mattostring(rot));
	settings.setValue("translation", mattostring((Mat) trans));

	/* not needed right now, perhaps in the future */
#if 0
	settings.setValue("essential",
			QString::fromStdString(mattostring(ess)));
	settings.setValue("fundamental",
			QString::fromStdString(mattostring(fun)));
	settings.setValue("mapx_left",
			QString::fromStdString(mattostring(mapxleft)));
	settings.setValue("mapy_left",
			QString::fromStdString(mattostring(mapyleft)));
	settings.setValue("mapx_right",
			QString::fromStdString(mattostring(mapxright)));
	settings.setValue("mapy_right",
			QString::fromStdString(mattostring(mapyright)));
#endif
	settings.endGroup();
}

bool CalibReview::validatePage()
{
	if (calibrated)
		return true;

	/* TODO disable Back button */

	/* run calibration in a new thread */
	calibrating = QtConcurrent::run(this, &CalibReview::calibrate,
			field("boardwidth").toInt(),
			field("boardheight").toInt(),
			field("squaresize").toInt());
	ui.status->setVisible(true);
	watchcalibrating.setFuture(calibrating);

	/* disable all controls */
	ui.prev->setEnabled(false);
	ui.next->setEnabled(false);
	ui.discard->setEnabled(false);
	ui.label1->setEnabled(false);
	ui.label2->setEnabled(false);
	ui.gridlines->setVisible(false);
	ui.exportsample->setVisible(false);
	ui.exportall->setVisible(false);

	emit completeChanged();
	repaint();

	return false;
}

bool CalibReview::isComplete() const
{
	/* if calibration is running */
	if (watchcalibrating.isRunning())
		return false;

	return imgs_left.size() >= MINSAMPLES;
}

/* next/previous/refresh the image and points buffers */
void CalibReview::cycle(int dir)
{
	const int w = field("boardwidth").toInt();
	const int h = field("boardheight").toInt();

	/* which direction to move in: -1 (left), 1 (right) or 0 (refresh) */
	if (dir < 0 && index > 0)
		index--;
	else if (dir > 0 && index < imgs_left.size() - 1)
		index++;
	else if (dir)
		return;

	/* disable the delete button if this is the last sample in the buffer */
	if (!watchcalibrating.isRunning() && !calibrated)
		ui.discard->setDisabled((imgs_left.size() == 1));

	ui.index->setText(QString::number(index + 1) + "/" +
			QString::number(imgs_left.size()));

	/* make copy of the img in case it is modified */
	Mat leftbuf, rightbuf;
	leftbuf = imgs_left[index].clone();
	rightbuf = imgs_right[index].clone();

	/* draw corners if checked */
	if (ui.gridlines->isChecked()) {
		corners_draw(imgs_left[index], pnts_left[index], w, h);
		corners_draw(imgs_right[index], pnts_right[index], w, h);
	}

	QPixmap pixleft = mattopixmap(imgs_left[index]);
	ui.left->setPixmap(pixleft.scaled(ui.left->size(),
				Qt::KeepAspectRatio));

	QPixmap pixright = mattopixmap(imgs_right[index]);
	ui.right->setPixmap(pixright.scaled(ui.right->size(),
				Qt::KeepAspectRatio));

	/* restore image without modifications */
	imgs_left[index] = leftbuf;
	imgs_right[index] = rightbuf;

	repaint();
}

/* delete the currently displayed sample from the buffers */
void CalibReview::discard()
{
	imgs_left.erase(imgs_left.begin() + index);
	pnts_left.erase(pnts_left.begin() + index);
	imgs_right.erase(imgs_right.begin() + index);
	pnts_right.erase(pnts_right.begin() + index);

	/* decrement index if this is the tail sample in the buffer */
	while (index + 1 > imgs_left.size())
		index--;

	/*
	 * disable the finish button if the amount of samples is less than
	 * the minimum
	 */
	emit completeChanged();

	/* refresh */
	cycle(0);
}
