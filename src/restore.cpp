/*
 * Create4Care - Erasmus MC, Rotterdam © 2020 <create4care@erasmusmc.nl>
 *
 * Copyright 2020 Bastiaan Teeuwen <bastiaan@mkcl.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <restore.h>

#include <QDir>
#include <QPushButton>

Restore::Restore(QWidget *parent) : QDialog(parent)
{
	ui.setupUi(this);
	this->setWindowFlags(this->windowFlags() &
			~Qt::WindowContextHelpButtonHint);
	setWindowModality(Qt::WindowModal);

	ui.buttons->button(QDialogButtonBox::Open)->setEnabled(false);
	connect(ui.sessions, &QListWidget::currentRowChanged, this, [=](int i) {
		if (i >= 0)
			session = ui.sessions->item(i)->data(Qt::DisplayRole)
					.toString();
		ui.buttons->button(QDialogButtonBox::Open)->setEnabled(i >= 0);
		ui.discard->setEnabled(i >= 0);
	});
	connect(ui.sessions, &QAbstractItemView::doubleClicked, this,
			&Restore::accept);

	connect(ui.discard, &QAbstractButton::clicked, this, [=] {
		QDir dir(settings.value("save/path").toString() + "/" +
				session);
		dir.removeRecursively();

		delete ui.sessions->selectedItems()[0];
	});

	connect(ui.buttons, &QDialogButtonBox::accepted, this,
			&Restore::accept);
	connect(ui.buttons, &QDialogButtonBox::rejected, this,
			&Restore::reject);

	if (!settings.contains("save/path")) {
		QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
		return;
	}

	QDir dir(settings.value("save/path").toString());

	/* empty path so restore button will be hidden if path is invalid */
	if (!dir.exists()) {
		settings.setValue("save/path", "");
		QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
		return;
	}

	QStringList dirs = dir.entryList(QStringList(), QDir::AllDirs |
			QDir::NoDotAndDotDot);

	/*
	 * check if directories are valid meaning the directory contains at
	 * least a left.jpg and right.jpg
	 */
	for (auto &item : dirs)
		if (dir.exists(item + "/left.jpg") &&
				dir.exists(item + "/right.jpg"))
			ui.sessions->addItem(item);

	ui.sessions->setCurrentRow(0);
}
