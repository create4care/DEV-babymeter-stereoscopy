\documentclass[12pt,a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage{newunicodechar}
\usepackage[english]{babel}

\usepackage[margin=1.0in]{geometry}
\usepackage[parfill]{parskip}
\tolerance=1000
\hyphenpenalty=1000
\hbadness=10000
\emergencystretch=2em
%\widowpenalties=1 10000
%\raggedbottom

\usepackage[defernumbers,sortcites,sorting=none]{biblatex}
\usepackage{booktabs}
\usepackage{csquotes}
\usepackage{enumitem}
\usepackage[toc]{glossaries}
\usepackage{graphicx, wrapfig}
\graphicspath{{./res/}}
\usepackage[bookmarks,colorlinks=true,linkcolor=black,citecolor=black,urlcolor=blue]{hyperref}
\usepackage{listings}
\usepackage{xcolor}

\definecolor{mGreen}{rgb}{0, 0.6, 0}
\definecolor{mGray}{rgb}{0.5, 0.5, 0.5}
\definecolor{mPurple}{rgb}{0.58, 0, 0.82}

\lstdefinestyle{cpp}{%
	commentstyle=\color{mGreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{mGray},
	stringstyle=\color{mPurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,
	breaklines=true,
	captionpos=b,
	keepspaces=true,
	numbers=left,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	tabsize=4,
	language=C
}

% serif
\usepackage{cochineal}
\usepackage[varqu,varl,var0]{zi4} % Inconsolata
\usepackage[scale=.95,type1]{cabin}
\usepackage[cochineal,bigdelims,cmintegrals,vvarbb]{newtxmath}
\usepackage[cal=boondoxo]{mathalfa}

\newcommand{\TITLE}{Stereo Vision with OpenCV}
\newcommand{\SUBTITLE}{Project Babymeter}
\newcommand{\AUTHOR}{
	\textbf{Author}\\
	Bastiaan Teeuwen (0945334)\\
	\\
	\textbf{Lecturers}\\
	S.M. Hekkelman\\
	H. Kroeze
}
\newcommand{\INSTITUTE}{Hogeschool Rotterdam}
\newcommand{\DATE}{\today}

\title{\TITLE{} --- \SUBTITLE}
\author{\AUTHOR}
\date{\DATE}

\addbibresource{bibliography.bib}

\makenoidxglossaries{}

\newglossaryentry{stereovision}{%
	name=stereo vision,
	description={The perception or construction of a 3D image based on the
	information from two eyes or images}
}

\newglossaryentry{nicu}{%
	name=NICU,
	description={Neonatal Intensive Care Unit; a device used to assist in
	the care and life support of prematurely born infants}
}

\newglossaryentry{relay}{%
	name=relay project,
	description={A project which has been worked on before and which is
	relayed to a new team after the first team's development cycle
	finishes. In Dutch: estafette project}
}

\newglossaryentry{opencv}{%
	name=OpenCV,
	description={An open source cross-platform library for computer vision}
}

\newglossaryentry{matlab}{%
	name=MATLAB,
	description={A proprietary programming language for numerical analysis
	and various other mathematical operations}
}

\newglossaryentry{bsd}{%
	name=BSD 2.0 licenced,
	description={An open source license that allows redistributing code,
	modifying the code (both of which we do in our project). In addition
	commericial use is also allowed}
}

\newglossaryentry{3dcloud}{%
	name=3D point cloud,
	description={A set of data points in a 3D space}
}

\newglossaryentry{triangulation}{%
	name=triangulation,
	description={The process of determining the location of a point in a (in
	our case) 3D point cloud by forming triangles to said point from points
	which are already known}
}

\newglossaryentry{undistortion}{%
	name=undistortion,
	description={The process of normalizing a set of points so they're no
	longer related to the intrinsic parameters of the camera}
}

\newglossaryentry{normalize}{%
	name=normalize,
	description={See \emph{\gls{undistortion}}}
}

\newglossaryentry{chessboard}{%
	name=chessboard pattern,
	description={A checkerboard pattern used in the calibration of cameras
	to extract 3D information from a 2D image}
}

\newglossaryentry{distortion}{%
	name=distortion,
	description={A twist, change or inconsistency in an image that makes it
	appear different from the way it really is}
}

\newglossaryentry{intrinsic}{%
	name=intrinsic parameters,
	description={Parameters describing a camera}
}

\newglossaryentry{extrinsic}{%
	name=extrinsic parameters,
	description={Parameters describing the transformation between a camera
	and the external world}
}

\newglossaryentry{rotmat}{%
	name=rotation matrix,
	description={A matrix used to perform rotation in a 3D Euclidean space}
}

\newglossaryentry{rectification}{%
	name=rectification,
	description={The process of simplifying the problem of finding matching
	points between 2 images}
}

\begin{document}

\begin{titlepage}

\raggedleft%

\rule{1pt}{\textheight}
\hspace{0.05 \textwidth}
\parbox[b]{0.75 \textwidth}{%
	{\huge \textbf{\TITLE}}\\[\baselineskip]
	{\Large \textbf{\SUBTITLE}}\\[3\baselineskip]
	{\large \AUTHOR}

	\vspace{0.5 \textheight}

	{\large \INSTITUTE}\\[\baselineskip]
	{\textit{\DATE}}\\[\baselineskip]
}

\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}
This report is a component of the final product set for ``Project 7/8:
Babymeter'' at \emph{Hogeschool Rotterdam} on behalf of \emph{Ronald van Gils}
from \emph{Create4Care} at \emph{Erasmus MC}. \emph{Create4Care} is an
initiative from the Erasmus MC hospital to solve practical problems within the
hospital using modern technology and innovations~\cite{create4care}. Our project
revolves around using computer \emph{\gls{stereovision}} to measure babies in a
Neonatal Intensive Care Unit (\emph{\gls{nicu}}), an intensive care unit used to
assist in the care of prematurely born infants~\cite{nicu}.

This project is a so called \emph{\gls{relay}}, meaning that a group from
Hogeschool Rotterdam worked on this project before. We are going to work on the
next iteration of the project and partially build our foundation, our final
product and final report on their results. From this point forward we will refer
to the previous team working on this project as the \emph{team from last year}.

\subsection{Acknowledgements}
I would like to give special thanks to \emph{David Kakes}, who has remained a
very enthusiastic member of the team throughout the whole project which was
really motivating for me to see. His diligence inspired me to work harder and
made me belief we could deliver a final product despite the loss of one of our
team members and the COVID-19 home quarantine.

Thanks to \emph{Ronald van Gils} and \emph{Timothy Singowikromo} for inspiring
us with new ideas every week and asserting our progress in a respectful and
professional manner.

Thanks to the \emph{Bram van Woerkens}, \emph{Meike Campen}, \emph{Merel Venema}
and \emph{Thomas van der Helm} from the study Industrial Design (Industrieel
Product Ontwerpen) at Hogeschool Rotterdam for assisting us in the development
of a user-friendly and user-oriented graphical user interface.
\newpage

\section{Executive summary}
\subsection{Current situation}
According to our client, the current technique for measuring babies in a NICU is
by using a large heavy metal caliper. For this operation a total of 2 nurses are
required; one to hold the baby in as still as possible and the other to operate
the caliper. What complicates things further is that babies in a NICU usually
have various rubes running over their bodies. Finally, this operation causes
some distress to the baby. We hope that not having to touch the baby nor the
NICU itself will prevent all these problems whilst still archives an comparable
or even more accurate measurement than with the caliper.

\subsection{Proposed solution}
Our client wants to experiment with \emph{\gls{stereovision}} to measure the
distance accurately and without touching the baby. This benefits the nurses, who
will not have to perform the delicate operation of inserting the caliper into
the NICU anymore.\\
In addition, this technique will prevent stress in the infant as no physical
intervention is required with the NICU nor the baby. Compared to other
techniques which have also been researched at Create4Care, like using a laser
scanner, stereo vision only requires two cameras and a device to process the
images. It is therefore also avoided that the flickering and light of lasers
disturbs the baby's fragile state.

\subsection{Technical description}
This document will cover the technical aspects of the stereo vision algorithm
using OpenCV\@. \emph{\gls{opencv}} is an open source (\emph{\gls{bsd}})
cross-platform library for computer vision. Functionality of OpenCV includes:
facial recognition, motion tracking, machine learning and stereo
vision~\cite{opencv}. When making the decision of what library to use for stereo
vision, the choice was easily made as OpenCV is widely known for having well
optimized, high performance code.

\subsection{Research topic}
We will research an efficient way to find the distance between two points in a
\emph{\gls{3dcloud}} using \emph{\gls{triangulation}} with OpenCV\@. This is
something that \emph{has} been done before in practice, but has not been clearly
documented as far as I've seen. We presume this is also why the previous group
chose to use \emph{\gls{matlab}} for this process, as the MATLAB website
documents this quite well~\cite{matlabstereo}. I hope that this document can be
of some help to other students and developers who want to develop a similar
application or contribute to our project.

\newpage
\section{Theoretical framework}
\subsection{Analysis of the current situation}
The team from last year achieved to perform triangulation using
\emph{\gls{matlab}}, a proprietary programming language for mathematical
operations including the generation of a point cloud from a set of stereo vision
images. Our client however clearly stated that the dependency on MATLAB is
quite disadvantageous because of several reasons:
\begin{enumerate}
	\item The software is proprietary and requires users to buy a costly
		license~\cite{matlab}
	\item A significant overhead is introduced as MATLAB is not optimized
		for performance~\cite{matlaboverhead}
	\item MATLAB is not cross-platform and therefore it is not suitable for
		running on an embedded platform, which the client would
		eventually like to do
\end{enumerate}
The team from last year attempted to come up with a solution for the
cross-platform issue introduced by MATLAB by using a client-server model. The
client would send a set of images from an embedded devices over the network to a
centralized server to process the data. The server would return the results and
the results are displayed.

This technique works in theory, but has several disadvantages in practice:
\begin{enumerate}
	\item The network operation brings a significant overhead to every
		request, even when ran locally (because the MATLAB server has to
		be started which takes quite long in practice).
	\item When ran on two separate machines, poor network infrastructure,
		bandwidth, QoS, server maintenance may limit the reliability of
		the software, which is not desirable in a medical environment.
\end{enumerate}

\subsection{Analysis of our options}
Our client is in favor of an open source approach to attract as much outside
attention as possible from researchers and developers alike. Hence we decided to
settle on OpenCV, which has the appropriate licensing, performance and
capabilities for the situation.

There are some smaller, often worse documented alternatives to OpenCV and
MATLAB's stereo vision capabilities. Despite having considered other libraries,
we still went with OpenCV, also because a team for computer science is
(according to our client) interested in integrating automatic point detection
using OpenCV's object detection functionality. Using the same library could
prove to be advantageous in later stages of development to reduce complexity and
keep the code more maintainable.

Apart from out switch from MATLAB to OpenCV for the image processing, we also
decided to write out code in C++ instead of Python, as the team from last year
did. Using Python can significantly improve development
speed~\cite{pythondevspeed}, at the cost of performance. We concluded that the
overhead that C++ development bring is negligible in comparison to the
performance that we require. Python tends to perform worse on stereo calibration
than C++ using their respective OpenCV bindings~\cite{pythonvscpp}. Especially
on an embedded platform (which we are supposed to take into account when writing
the triangulation algorithm), it is of importance that performance will not be
an issue.

\subsection{Literary research}
There is a wide variety of (online) resources about the mathematics of
performing triangulation~\cite{trimath}~\cite{stereomath}~\cite{whatstereo}. But
there is not really a good, reputable source, that is up-to-date with the
current capabilities of the calib3d module (the module that contains stereo
vision functions) from OpenCV\@. The best source of information is the official
OpenCV documentation (version 4.3.0 at the moment of writing)~\cite{calib3d}.
This source also provides a description of the process calibrating cameras for
stereo vision which is quite hard to understand for the average programmer. I
therefore also hope to make my article as accessible as possible for the average
C++ programmer as I --- as a non-mathematician --- spent way to much time trying
to decipher the official documentation.

Several books on Computer Vision and Stereo Vision have also been written. The
next section covers which resources exactly will be used in the fabrication of
this report.

\newpage
\section{Methodology}
\subsection{Resources}
We will be mostly referring to the official OpenCV documentation as this is the
most reliable source, containing the most up-to-date information on the
libraries included in OpenCV\@. Besides that, some research into the inner
workings of the inner workings of cameras, different types of cameras,
algorithms for triangulation and calculating the distance between points in a 3D
space will have to be done.

Besides the official documentation, the book `Computer Vision: A Reference
Guide' by Zhang, Zhengyou~\cite{Zhang2014} will also serve as a reference on the
intrinsics of stereoscopy. Zhengyou Zhang is widely known in the Computer Vision
community and has layed the foundation for several algorithms related to camera
calibration which have also been implemented in OpenCV\@.

\subsection{Testing}
I will be testing my algorithm by comparing the output to real world values. The
client desires a systematic accuracy of 1mm. For example, the algorithm outputs
a distance of 4mm between points $A$ and $B$ and the real world value is 4.5mm
when the points are aligned pixel perfect. In this case the test has succeeded,
because the accuracy of 1mm has been archived~\cite{david}.

\newpage
\section{Results}
\subsection{Calibration}
Before triangulation can take place, the two cameras that will be used in the
process will be have to be calibrated individually. This is a fairly
well-documented process that has been performed many times before by people
working with OpenCV and computer vision in general~\cite{camcalib}. Calibration
is necessary to be able to extract 3D information from a 2D image taken by the
calibrated camera in question.

In Figure~\ref{fig:setup}, a stereoscopic setup is illustrated including the
relevant parameters which will be referenced and explained in the sections
below.

\begin{figure}[h]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{stereosetup}
		\caption{Stereoscopic camera setup}\label{fig:setup}
	\end{center}
\end{figure}

\subsubsection{Mapping control points}
\begin{wrapfigure}{r}{5cm}
	\includegraphics[width=5cm]{checkerboard}
	\caption{A chessboard pattern}\label{fig:check}
\end{wrapfigure}

A crucial part of calibration is the so called mapping of \emph{control points},
a process in which a couple real world targets that form the correspondence
between the real world and the image captured by the camera~\cite[p.~424]{pdm}.

There are a total of 2 different techniques available for performing this
operation in OpenCV, namely using a so called \emph{\gls{chessboard}} (see
Figure~\ref{fig:check}) or \emph{circleboard pattern}. Some parties claim the
chessboard pattern yields more accurate
results~\cite{chessvscircle}~\cite[p.~424]{pdm}, whilst others claim circleboard
yield more accurate results and is better at handling
\gls{distortion}~\cite{circleisbetter}.

We have decided to go with the chessboard pattern, as it is best documented,
most often cited and --- despite the controversy --- this seems to be the
de facto standard~\cite[p.~424]{pdm}.

According to the documentation, there is a single function that takes an input
image, the dimensions of the number of squares in the chessboard and outputs an
array containing the coordinates of the corners of the chessboard, which is
exactly what we need:
\lstinline[style=cpp]{cv::findChessboardCorners()}~\cite{findchessboardcorners}.
See the code excerpt from our project on the next page to see how the function
can be used.

\begin{lstlisting}[style=cpp]
int corners_find(cv::Mat &img, std::vector<cv::Point2f> &points, int w, int h)
{
	/* find a chessboard pattern in the supplied image */
	const int flags = cv::CALIB_CB_ADAPTIVE_THRESH |
			cv::CALIB_CB_NORMALIZE_IMAGE | cv::CALIB_CB_FAST_CHECK;
	if (!cv::findChessboardCorners(img, cv::Size(w, h), points, flags))
		return 0;

	/* improve the found corners' coordinate accuracy */
	cv::Mat grayimg;
	cv::cvtColor(img, grayimg, cv::COLOR_BGR2GRAY);
	cv::cornerSubPix(grayimg, points, cv::Size(11, 11), cv::Size(-1, -1),
			TermCriteria(cv::TermCriteria::EPS +
			cv::TermCriteria::COUNT, 30, 0.0001));

	return 1;
}
\end{lstlisting}

Note that we also apply the \lstinline[style=cpp]{cv::cornerSubPix()} algorithm
to improve accuracy at the cost of a negligible performance
overhead~\cite[p.~440]{pdm}.

\subsubsection{Camera calibration}\label{sec:obj}
The next step is to perform the actual calibration, a process which will return
us the \emph{\gls{intrinsic}} and \emph{\gls{extrinsic}} relating to one camera.
Intrinsic parameters describe parameters relating to the camera itself: the
focal length $f$ and the principal point $c$~\cite[p.
3--27]{geom}~\cite{Zhang2014}:

\begin{center}
\begin{equation*}
\begin{bmatrix}
	f_x & 0 & c_x\\
	0 & f_y & c_y\\
	0 & 0 & 1
\end{bmatrix}
\end{equation*}
\end{center}

Extrinsic parameters on the other hand describe the relation between the camera
and the external world: the rotation $R$ and translation $T$ of the focal point
of the camera in relation with the principal point~\cite{Zhang2014}. Rotation is
defined as a $3\times3$ \gls{rotmat} $R$. Translation is defined as a $3\times1$
vector $T$. When $R$ and $T$ are combined, you can calculate the position of the
camera in real world coordinates.

The calibration of cameras is time-consuming process in our application, as a
set of at least 10 images (though an average of 25 is recommended) for both left
and right cameras have to be processed to guarantee the desired systematic
accuracy of 1mm~\cite{david}.

The following excerpt utilizes the
\lstinline[style=cpp]{cv::calibrateCamera()}~\cite{calibratecamera} function
with as input the chessboard corners and the size of the image,
\lstinline[style=cpp]{obj} contains a matrix with the chessboard pattern in real
world coordinates:

\begin{lstlisting}[style=cpp]
	cv::Mat cam;                     /* camera matrix */
	cv::Mat dis;                     /* distortion coefficients */
	std::vector<cv::Mat> rmat, tvec; /* rotation and translation matrix */

	/* calibrate the camera's intrinsic and extrinsic parameters */
	const int flags = cv::CALIB_FIX_K4 | cv::CALIB_FIX_K5;
	cv::calibrateCamera(obj, points, size, cam, dis, rmat, tvec, flags);
\end{lstlisting}

\subsubsection{Stereoscopic calibration}
In order to perform stereoscopic calibration, both cameras have to be
individually calibrated first: meaning that their \emph{\gls{intrinsic}} and
\emph{\gls{extrinsic}} are known. These parameters serve as input for our next
algorithm.

We will calculate a new rotation matrix $R^s$ and translation vector $T^s$ which
describes the angle and translation between two cameras' focal points
respectively~\cite{markus2004}. Because we already have our input data, this
process is very straightforward

\begin{lstlisting}[style=cpp]
	cv::Mat rot;     /* rotation matrix */
	cv::Vec3d trans; /* translation vector */
	cv::Mat ess;     /* essential matrix (ignored) */
	cv::Mat fun;     /* fundamental matrix (ignored) */

	/* calculate stereoscopic parameters */
	cv::stereoCalibrate(obj, pntsleft, pntsright, camleft, disleft,
			camright, disright, size, rot, trans, ess, fun);
\end{lstlisting}

\subsubsection{Rectification}
\begin{wrapfigure}{r}{5cm}
	\includegraphics[width=5cm]{rectification}
	\caption{Rectification}\label{fig:rect}
\end{wrapfigure}

These newly calculated parameters $R^s$ and $T^s$ will serve as input for a
\emph{\gls{rectification}} algorithm. Rectification serves to provide yet
another rotation matrix $R^r$ in addition to a projection matrix $P^r$ which be
used as input for the triangulation algorithm. Rectification is the process of
finding the parameters needed to map 2 stereoscopic images to the same plane
(see Figure~\ref{fig:rect}). This greatly simplifies the problem of
triangulation~\cite{rect}. This step can be skipped (even though you really
shouldn't because the performance overhead is minimal), but will yield less
accurate results when the actual triangulation is performed.

This same algorithm we use for stereoscopic rectification is also often used in
a modified form to fix warps or visual inconsistencies in images. OpenCV
internally uses the Bouguet algorithm~\cite{bouguet}, which is based on another
algorithm in a book used as main reference for writing this report: `Computer
Vision: A Reference Guide' by Zhang, Zhengyou~\cite{Zhang2014}.

Just like stereoscopic calibration, rectification is a fairy straightforward
process using OpenCV\@:

\begin{lstlisting}[style=cpp]
	cv::Mat rotleft, rotright; /* rotation matrices */
	cv::Mat proleft, proright; /* projection matrices */
	cv::Mat dtdmap;            /* disparity-to-depth map (ignored) */

	/* compute the rectification transformations */
	cv::stereoRectify(camleft, disleft, camright, disright, size, rot,
			trans, rotleft, rotright, proleft, proright, dtdmap);
\end{lstlisting}

\newpage
\subsection{Triangulation}
Let's define point $A$ and $B$ in real world 3D coordinates, with their
corresponding 2D pixel coordinates on image 1 and 2 which have been
predetermined: $A_1$, $A_2$, $B_1$ and $B_2$. We want to calculate the distance
between point $A$ and $B$ in real world units but only have access to their
respective pixel coordinates. This is where triangulation comes in.

Triangulation is the process of determining the location of a point in (in our
case) a 3D point space by forming triangles to said point from adjacent point
which are already known. OpenCV does this using the Least Squares algorithm, a
simple yet efficient and effective algorithm~\cite{hartley2004}.

All parameters that have been obtained during calibration will serve as input
for triangulation. These are the parameters we will be using for triangulation:
\begin{description}[align=right]
	\item [$C$] Intrinsic parameter matrix
	\item [$D$] Distortion coefficients
	\item [$R$] The stereoscopic rotation matrix (rotation between the
		cameras' focal points)
	\item [$P$] The stereoscopic projection matrix (distance between the
		cameras' focal points)
\end{description}

\subsubsection{Undistortion}
First of all, we have to \emph{\gls{normalize}} the coordinates using
\lstinline[style=cpp]{cv::undistortPoints()}, \emph{\gls{undistortion}} is the
the same as normalizing the points in our case, meaning that they are no longer
related to the intrinsic parameters of the camera~\cite{undistortpoints}. We do
this for both left and right images:

\begin{lstlisting}[style=cpp]
	/* pixel coordinates */
	std::vector<cv::Point2f> coordsleft, coordsright;

	...

	Mat ucoordsleft, ucoordsright; /* undistorted coordinates */

	cv::undistortPoints(coordsleft, ucoordsleft, camleft, disleft,
			rotleft, proleft);
	cv::undistortPoints(coordsright, ucoordsright, camright, disright,
			rotright, proright);
\end{lstlisting}

Note that both input and output coordinates are $2\times{}N$ vectors where $N$
is the number of points that we want to calculate. $N = 1$ in our case for both
left and right inputs and outputs.

\newpage
\subsubsection{Triangulation}
\emph{\gls{triangulation}} can now be performed. We do this using
\lstinline[style=cpp]{cv::triangulatePoints()}~\cite{triangulatepoints} which
will output 4D real world coordinates in the projective coordinate system, which
is not ideal. We therefore also have to convert the coordinates into the 3D
coordinate system used in the Euclidian space: the Cartesian coordinate
system~\cite{homo}:

\begin{lstlisting}[style=cpp]
	cv::Mat coords4d; /* projective coordinates in a 4xN matrix */
	cv::Mat coords3d; /* projective and cartesian coordinates */

	cv::triangulatePoints(proleft, proright, ucoordsleft, ucoordsright,
			coords4d);

	/* convert into cartesian coordinates */
	cv::transpose(coords4d, coords3d);
	cv::convertPointsFromHomogeneous(coords3d, coords3d);
\end{lstlisting}

\subsubsection{Distance between 2 3D points}
As the 3D Cartesian world coordinates of points $A$ and $B$ are now known, all
that's left to do is to calculate the distance between the two. For this we
simply use the Pythagorean theorem twice, once for the $x$ and $y$ coordinates
and again for the result of that and the $z$ coordinate:

\begin{center}
	$d(A, B) = \sqrt{{(\sqrt{{(A_x - B_x)}^2 + {(A_y - B_y)}^2})}^2 + {(A_z - B_z)}^2}$
\end{center}

The units of the output depend on the units used to describe
\lstinline[style=cpp]{obj} (see also section~\ref{sec:obj}).

\newpage
\section{Conclusion}
The goal of this report is to research an efficient way to find the distance
between two points in a \emph{\gls{3dcloud}} using \emph{\gls{triangulation}}
with OpenCV\@.

A rough yet much saying comparison between the software written by the group
from last year in MATLAB and Python (\emph{Babymeter 2}) and our version using
OpenCV and C++ (\emph{Babymeter 3}) on comparable hardware running Linux:

\begin{center}
\begin{tabular}{c c c}
	& Babymeter 2 & Babymeter 3\\
	\midrule
	Calibration (with 20 photos) & 6.4s & 1.2s\\
	Triangulation & 7.5s & 0.02s
\end{tabular}
\end{center}

Because the overhead from the server-client model between MATLAB and Python in
addition to the inefficiency of MATLAB itself have been eliminated, performance
is significantly better.

\subsection{Recommendations}
Based on my results and above comparison, I recommend using OpenCV as
Stereoscopic Vision library because of its high efficiency and performance
codebase. Despite its high performance, the documentation is relatively well
laid out and easy to read though some matrices, stereoscopy and computer vision
have to be done in advance.

In addition I recommend writing the application in C++, also due to its high
performance. OpenCV also has bindings for Java (which is out of the question due
to poor documentation and performance concerns), in addition to Python (which
also introduces a significant overhead)~\cite{pythonvscpp}. Python is an quick
and easy language and to develop software in. But also because this software
will be used in a medical environment, we want to avoid the complexity that
high-level languages introduce to provide a stable and reliable yet portable
application.

\subsection{Future research recommendations}
During my research, I've stumbled upon several functions in the OpenCV library
that may be interesting to research further in future iterations of this
project. Namely:
\begin{enumerate}
	\item \lstinline[style=cpp]{cv::findChessboardCornersSB()}: A function
		which claims to be better at handling image noise/distortions
		(which could be useful too in darker lighting conditions
		perhaps), be faster when processing large images and return more
		accurate coordinates~\cite{findchessboardcornerssb}
	\item \lstinline[style=cpp]{cv::calibrateCameraRO()}: A function which
		claims to perform a more accurate camera
		calibration~\cite{calibratecameraro}
	\item Using the CUDA libraries OpenCV provides to accelerate calibration
		on NVIDIA platforms~\cite{cuda} (like the Jetson Nano which we
		use as main hardware component~\cite{david})
\end{enumerate}
These, among other future ideas for our final product can be found on
our online \href{https://trello.com/b/TvzwNz9R/scrum-board}{Scrum Board}.

\newpage
\printnoidxglossaries{}

\newpage
\addcontentsline{toc}{section}{References}
\printbibliography{}

\end{document}
