\documentclass[11pt,a4paper]{article}

\usepackage[T1]{fontenc}
\usepackage{newunicodechar}
\usepackage[english]{babel}

\usepackage[margin=1.0in]{geometry}
\usepackage[parfill]{parskip}
\tolerance=1000
\hyphenpenalty=1000

\usepackage[colorlinks = true,
            linkcolor = blue,
            urlcolor  = blue,
            citecolor = blue,
            anchorcolor = blue]{hyperref}
\usepackage{enumitem}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,breaklines=true}
\usepackage[dvipsnames]{xcolor}

% serif
\usepackage{cochineal}
\usepackage[varqu,varl,var0]{zi4} % Inconsolata
\usepackage[scale=.95,type1]{cabin}
\usepackage[cochineal,bigdelims,cmintegrals,vvarbb]{newtxmath}
\usepackage[cal=boondoxo]{mathalfa}

\title{Project Management}
\author{Bastiaan Teeuwen}
\date{\today}

\begin{document}

\maketitle

This document describes how agreements, cooperation, software version-control,
software development and planning will be implemented in this project. If you
wish to contribute any changes to this document, please contact the project
leader/Scrum Master directly or submit a pull request with a description of the
desired adjustments.

\section{General agreements}\label{sec:agreements}
\begin{itemize}
	\item Every team member shall actively engage in the project and
		contribute to a pleasant and wholesome environment to work in.
	\item If you feel like you or another team member is not performing
		optimally, contact the project leader immediately so we can
		resolve the issue as soon as possible.
	\item Interpersonal conflicts between team members may be mentioned to
		the project leader so we can openly discuss the issue with the
		whole team (if this is desired and applicable to the situation).
	\item If you do not show up without notifying the team, a warning will
		be issued by the project leader, the second time you don't show
		up, your absence will be discussed with the Product Owner.
	\item Availability on either \emph{WhatsApp} or \emph{e-mail} is
		expected, online absence or ignoring directly addressed messages
		is only tolerated for a maximum of 2 working days after which a
		warning will be issued by the project leader. If you are absent
		a second time or are unreachable for a significant amount of
		time (longer than 4 working days), the project leader will
		contact the Product Owner.  If you wish to suggest another means
		of communication or if you are missing any team member's contact
		information, you may ask the project leader.
	\item When a team member is assigned responsibility for a certain task,
		he/she may still collaborate with other members. However, The
		final responsibility remains with the assigned team member.
	\item Costs made outside of the assigned budget need to be reported to
		the project leader who will keep track of the expenses. The
		project leader will spread the costs over the whole team.
	\item Unannounced expenses outside of the assigned budget \emph{cannot}
		be declared with the project leader unless the whole team agrees
		otherwise.
	\item All code will be licensed under the \emph{Apache License 2.0} and
		therefore all libraries that are used have to be compatible.
		Team members are not allowed to share code with a third-party
		unless the \emph{client} says so.
	\item If you notice that any team member refuses to comply with the
		general agreements described above, you may contact either the
		\emph{project leader} or the \emph{Product Owner}.
\end{itemize}

\section{Software version control}
For version control, git will be used. This is what everyone is our team is
familiar with and also the de-facto standard for version control in bussiness
environments.

Our git repository can be cloned from:
``\texttt{git@gitlab.com:teeuwen/babymeter.git}'' or can be access via a web
interface \href{https://gitlab.com/teeuwen/babymeter}{online}.

\section{Software development}
For this project, we will be applying \emph{Scrum} in our development process.

The various roles in our project include:
\begin{itemize}
	\item Our \textbf{Scrum Master} is \emph{Bastiaan Teeuwen}
		<\href{mailto:bastiaan@mkcl.nl}{bastiaan@mkcl.nl}>.
	\item The \textbf{Product Owner}(s) in our project are the teachers that
		guide and teach this subject, namely \emph{H. (Hugo) Kroeze}
		<\href{mailto:h.kroeze@hr.nl}{h.kroeze@hr.nl}> and \emph{S.M.
		(Sandra) Hekkelman}
		<\href{mailto:s.m.hekkelman@hr.nl}{s.m.hekkelman@hr.nl}>.
	\item Our \textbf{Client} is \emph{R.H.J. (Ronald) van Gils}
		<\href{mailto:r.h.j.van.gils@hr.nl}{r.h.j.van.gils@hr.nl}>.
\end{itemize}

We will be working in collaboration with \emph{Industrial Design} from
Hogeschool Rotterdam by supervision of the \emph{client}. The communication will
happen via \emph{WhatsApp} and \emph{e-mail}. The same clauses described in
Section~\ref{sec:agreements} apply to this collaboration, even though they are
\emph{not} strictly enforced.

% TODO

We will be using \emph{two} boards, which can be accessed
\href{https://trello.com/babiesco2}{online} (only accessible to team members)
and are described in the sections below.

\subsection{Product Backlog and Scrum Board}
This board contains our project backlog, a list of global requirements for our
project determined both by us and by our client. When the project starts, the
list will be filled with everything we know the final product will require. The
product backlog is dynamic, meaning new entries may be added to the list after
development has started.

Another name for the entries on this list are \emph{User Stories}. Every story
has a description answering at least the answer to the questions; for
\emph{who}m something is developed, \emph{what} needs to be developed and
finally \emph{why} it needs to be developed.

This board consists of 7 lists (from left to right):
\begin{itemize}[align=left, leftmargin=9em, labelsep=0pt, labelwidth=9em]
	\item[- \textbf{Unfinished Stories}] User Stories that aren't yet
		completed, don't meet the descriptive requirements for a User
		Story or are vague and need to be revised.
	\item[- \textbf{Product Backlog}] User Stories that are finished and may
		not be changed unless discussed with the team. Entries in this
		list are not yet in development.
	\item[- \textbf{Sprint Backlog}] User Stories that will be worked on in
		this sprint need to be moved into this list.
	\item[- \textbf{To-Do}] Contains the tasks that will be worked on in
		this sprint; these are based on, but \emph{NOT} the same as User
		Stories.
	\item[- \textbf{In Progress}] Tasks that are currently being worked on.
	\item[- \textbf{Testing}] Tasks that are currently being tested.
	\item[- \textbf{Sprint \textit{x}: Done}] When a task or User Story is
		complete, it is moved to this list. After a sprint is finished,
		the list is archived.
\end{itemize}

Stories in the \emph{User Stories} or \emph{Sprint Stories} must also have one
of the following labels categorizing them according to the \emph{MoSCoW} method:
\begin{itemize}[align=left, leftmargin=7em, labelsep=0pt, labelwidth=7em]
	\item[\textbf{\color{Blue} Must have}] Critical stories that are
		absolutely critical to completion of the project. These stories
		\emph{must} be finished when the project ends.
	\item[\textbf{\color{OliveGreen} Should have}] These stories are
		important but not critical to delivering a working product.
	\item[\textbf{\color{YellowOrange} Could have}] These stories are
		desirable, but again, not critical to delivering a working
		product. Should only be taken into design if the team has free
		time on their hands.
	\item[\textbf{\color{Maroon} Won't have}] Not used in this project.
\end{itemize}

Both \emph{tasks} and \emph{User Stories in the Sprint Backlog} always need to
have one or more team members assigned.

Stories in the scrum board may not be moved to another list outside of scrum
meetings or unless discussed with the whole team.

\section{Planning}
\emph{Scrum meetings} will take place every week, on Monday (not determined yet) % TODO define

\emph{Scrum reviews} will happen every other week on \emph{Thursdays}. On this
day, the product owner will review our progress and sprint planning.

For more information on our planning, refer to the
\href{https://calendar.google.com/calendar/b/1?cid=aHIubmxfaDE0YzlncHZpbmFqamF1c2Y1dHJvcjQ3aDRAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ}{project
calendar} (only accessible to team members)

\section{Standards}
\begin{itemize}
	\item Every document that is to be included in the final report or final
		product has to be written in United States English.
	\item Every presentation, the scrum board and the product backlog are to
		be written in Dutch as these will be shared with the product
		owner and the client.
	\item Every document has to be written in LaTeX using the template
		provided in ``\texttt{doc/template.tex}''.
	\item Like mentioned in Section~\ref{sec:agreements}, all code will be
		licensed under the \emph{Apache License 2.0}.
\end{itemize}

\end{document}
