#include <Servo.h>

//motoren aanmaken
Servo myservo_1;  
Servo myservo_2; 
Servo myservo_3; 
Servo myservo_4; 

// begin posities van motoren
int pos=80;
int pos_2=80;
int pos_3=80;
int pos_4=80;

//draaihoeken
int draai_hoek_65=65;
int draai_hoek= 50;
int draai_hoek_35= 35;
int draai_hoek_25= 25;
int draai_hoek_20= 20;
int draai_hoek_15= 15;

//pauze
int Speed=4000;

// joystick x en y waarde
int value_x = 0;
int value_y = 0;

int stand=0;


int led =8;

int position_1=0;
int position_2=0;
int position_3=0;
int position_4=0;

int moveStep=1
;
void setup() {
Serial.begin(9600);
pinMode(led,OUTPUT);

myservo_1.attach(9);
myservo_2.attach(10);
myservo_3.attach(11);      
myservo_4.attach(12); 

resetLed();//methode die zorgt ervoor dat de led op de knop knippert.

}
void(* resetFunc) (void) = 0;// methode voor rest 
void demo();


void loop() {
  if(stand==0)
 {
 myservo_1.write(pos);
 myservo_2.write(pos_2);
 myservo_3.write(pos_3);
 myservo_4.write(pos_4);
 }

//analog input van joystick lezen
value_y = analogRead(A0);//y
value_x = analogRead(A1); //x

//data lezen uit seriele communicatie 
char text = Serial.read();

//Joystick-DOWN : methode cotrol aanroepen. Gebruiker kan de positie zelf bepalen
if(value_y == 1023)
  {
  control();
  }

 ////Joystick-UP :methode demo aanroepen.
 else if(value_y == 0)
  {
    Serial.println("Hi, Im demo");
    new_demo(); 
  }

 //Joystick-LEFT :methode previous aanroepen.
 else if(value_x == 1023 || text == '2' ) //data  "text" komt uit seriele port, op deze manier kan die ook aansturen via pc 
  {
    text =0;// rest de data
    previous();
    }

//Joystick-RIGHT :methode next aanroepen.    
else if(value_x == 0 || text == '1')
  {
    text =0;
    next();
   }

    else if(text == '3')
  {
    resetFunc();
  }
  
 else
   { }
   
}
   

void resetLed()
{
 digitalWrite(led,HIGH);
 delay(500);
 digitalWrite(led,LOW);
 delay(500);
 digitalWrite(led,HIGH);
 delay(500);
 digitalWrite(led,LOW);

  }
  
void next()
{
stand+=1; // 1 klik betekent naar volgende stand
   if(stand >20)
   {
    resetFunc();//totaal zijn er 20 standen (limiet), dan wordt reset functie aangeroepen
    }
    else{
    }
    alleStanden();//zoek die stand op in switch en dan uitvoeren
    delay(500);
 }

 void previous() 
 {
  stand-=1;// 1 klik betekent naar vorige stand
   if(stand <0)
   {
    resetFunc();
    }
    else{
    }
    alleStanden();
    delay(500);
 }
 
 void control()
 {
  digitalWrite(led,HIGH);
  delay(2000);
  position_1=80;
  position_2=80;
  position_3=80;
  position_4=80;
  myservo_1.write(position_1);
  myservo_2.write(position_2);
  myservo_3.write(position_3);
  myservo_4.write(position_4);

  
  boolean state=true;
  while(state==true){
  value_y = analogRead(A0);//y
  value_x = analogRead(A1); //x
  
  //down
  if(value_y == 1023)
  {
    
  position_3-=moveStep;
  position_4+=moveStep;
  if(position_3>0&&position_3<160 &&position_4>0&&position_4<160)
  {
 
  myservo_3.write(position_3);
  myservo_4.write(position_4);
  Serial.print(position_3);
  Serial.print(" and ");
  Serial.print(position_4);
  Serial.println("\n\n");
  }
  else{
  position_3+=moveStep;
  position_4-=moveStep;
    }
  
  }
  //  up
  else if(value_y == 0)
  {
  position_3+=moveStep;
  position_4-=moveStep;
  if(position_3>0&&position_3<160 &&position_4>0&&position_4<160)
  {
  myservo_3.write(position_3);
  myservo_4.write(position_4);
  Serial.print(position_3);
  Serial.print(" and ");
  Serial.print(position_4);
  Serial.println("\n\n");
   }
   else
   {
    position_3-=moveStep;
    position_4+=moveStep;
  }
  }
  else if(value_x == 1023)
  {
  position_1-=moveStep;
  position_2+=moveStep;
  if(position_1>0&&position_1<160 &&position_2>0&&position_2<160)// range van de hoeken zijn 1 tot 159
  {
 
  myservo_1.write(position_1);
  myservo_2.write(position_2);
  Serial.print(position_1);
  Serial.print(" and ");
  Serial.print(position_2);
  Serial.println("\n\n");
  }
  else{
  position_1+=moveStep;
  position_2-=moveStep;
    }
  }
    
      else if(value_x == 0)
  {
  position_1+=moveStep;
  position_2-=moveStep;
  if(position_1>0&&position_1<160 &&position_2>0&&position_2<160)
  {
 
  myservo_1.write(position_1);
  myservo_2.write(position_2);
  Serial.print(position_1);
  Serial.print(" and ");
  Serial.print(position_2);
  Serial.println("\n\n");
  }
  else{
  position_1-=moveStep;
  position_2+=moveStep;
    }
   }
  
  delay(50);
}
 }  


 
 
  


void alleStanden()
{
  switch(stand)
{
  case 0:
  Serial.println("0");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 1:
  Serial.println("1");
  myservo_1.write(55);
  myservo_2.write(105);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 2:
  Serial.println("2");
  myservo_1.write(30);
  myservo_2.write(130);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 3:
  Serial.println("3");
   myservo_1.write(15);
  myservo_2.write(145);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 4:
  Serial.println("4");
  myservo_1.write(105);
  myservo_2.write(55);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 5:
  Serial.println("5");
  myservo_1.write(130);
  myservo_2.write(30);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
  case 6:
  Serial.println("6");
  myservo_1.write(145);
  myservo_2.write(15);
  myservo_3.write(80);
  myservo_4.write(80);
  break;
    case 7:
  Serial.println("7");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(55);
  myservo_4.write(105);
  break;
  case 8:
  Serial.println("8");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(30);
  myservo_4.write(130);
  break;
  case 9:
  Serial.println("9");
  myservo_1.write(80);
  myservo_2.write(80);
   myservo_3.write(15);
  myservo_4.write(145);
  break;
  case 10:
  Serial.println("10");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(105);
  myservo_4.write(55);
  break;
  case 11:
  Serial.println("11");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(130);
  myservo_4.write(30);
  break;
  case 12:
  Serial.println("12");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(145);
  myservo_4.write(15);
  break;
  case 13:
  Serial.println("13");
  myservo_1.write(65);
  myservo_2.write(95);
  myservo_3.write(65);
  myservo_4.write(95);
  break;
  case 14:
  Serial.println("14");
  myservo_1.write(45);
  myservo_2.write(115);
  myservo_3.write(45);
  myservo_4.write(115);
  break;
    case 15:
  Serial.println("15");
  myservo_1.write(95);
  myservo_2.write(65);
  myservo_3.write(65);
  myservo_4.write(95);
  break;
  case 16:
  Serial.println("16");
  myservo_1.write(115);
  myservo_2.write(45);
  myservo_3.write(45);
  myservo_4.write(115);
  break;
  case 17:
  Serial.println("17");
  myservo_1.write(65);
  myservo_2.write(95);
  myservo_3.write(95);
  myservo_4.write(65);
  break;
  case 18:
  Serial.println("18");
  myservo_1.write(45);
  myservo_2.write(115);
  myservo_3.write(115);
  myservo_4.write(45);
  break;
  case 19:
  Serial.println("19");
  myservo_1.write(95);
  myservo_2.write(65);
  myservo_3.write(95);
  myservo_4.write(65);
  break;
  case 20:
  Serial.println("20");
  myservo_1.write(115);
  myservo_2.write(45);
  myservo_3.write(115);
  myservo_4.write(45);
  break;
  default:
 Serial.println("reset");
 break;
  }
}


void new_demo(){
digitalWrite(led,HIGH);
myservo_1.write(pos);
myservo_2.write(pos_2);
myservo_3.write(pos_3);
myservo_4.write(pos_4);  
delay(2000);
 Serial.println("1");
  myservo_1.write(55);
  myservo_2.write(105);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("2");
  myservo_1.write(30);
  myservo_2.write(130);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("3");
   myservo_1.write(15);
  myservo_2.write(145);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("4");
  myservo_1.write(105);
  myservo_2.write(55);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("5");
  myservo_1.write(130);
  myservo_2.write(30);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("6");
  myservo_1.write(145);
  myservo_2.write(15);
  myservo_3.write(80);
  myservo_4.write(80);
  delay(Speed);
  Serial.println("7");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(55);
  myservo_4.write(105);
  delay(Speed);
  Serial.println("8");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(30);
  myservo_4.write(130);
  delay(Speed);
  Serial.println("9");
  myservo_1.write(80);
  myservo_2.write(80);
   myservo_3.write(15);
  myservo_4.write(145);
  delay(Speed);
  Serial.println("10");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(105);
  myservo_4.write(55);
  delay(Speed);
  Serial.println("11");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(130);
  myservo_4.write(30);
  delay(Speed);
  Serial.println("12");
  myservo_1.write(80);
  myservo_2.write(80);
  myservo_3.write(145);
  myservo_4.write(15);
  delay(Speed);
  Serial.println("13");
  myservo_1.write(65);
  myservo_2.write(95);
  myservo_3.write(65);
  myservo_4.write(95);
  delay(Speed);
  Serial.println("14");
  myservo_1.write(45);
  myservo_2.write(115);
  myservo_3.write(45);
  myservo_4.write(115);
  delay(Speed);
  Serial.println("15");
  myservo_1.write(95);
  myservo_2.write(65);
  myservo_3.write(65);
  myservo_4.write(95);
  delay(Speed);
  Serial.println("16");
  myservo_1.write(115);
  myservo_2.write(45);
  myservo_3.write(45);
  myservo_4.write(115);
  delay(Speed);
  Serial.println("17");
  myservo_1.write(65);
  myservo_2.write(95);
  myservo_3.write(95);
  myservo_4.write(65);
  delay(Speed);
  Serial.println("18");
  myservo_1.write(45);
  myservo_2.write(115);
  myservo_3.write(115);
  myservo_4.write(45);
  delay(Speed);
  Serial.println("19");
  myservo_1.write(95);
  myservo_2.write(65);
  myservo_3.write(95);
  myservo_4.write(65);
  delay(Speed);
  Serial.println("20");
  myservo_1.write(115);
  myservo_2.write(45);
  myservo_3.write(115);
  myservo_4.write(45);
  delay(Speed);
   resetFunc();
  }





