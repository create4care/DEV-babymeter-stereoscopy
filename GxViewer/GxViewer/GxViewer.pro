
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

LIBS += -lgxiapi \

TARGET = GxViewer
TEMPLATE = app

unix:!mac:QMAKE_LFLAGS += -L/usr/lib -L./ -Wl,--rpath=.:/usr/lib

INCLUDEPATH += ./include/

SOURCES += main.cpp\
    GxViewer.cpp \
    AcquisitionThread.cpp \
    Fps.cpp \
    Common.cpp

HEADERS += $$files(./include/*.h)

HEADERS  += \
    GxViewer.h \
    AcquisitionThread.h \
    Fps.h \
    Common.h

FORMS    += \
    GxViewer.ui

