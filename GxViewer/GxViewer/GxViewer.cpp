//--------------------------------------------------------------------------------
/**
\file     GxViewer.cpp
\brief    CGxViewer Class implementation file

\version  v1.0.1807.9271
\date     2018-07-27

<p>Copyright (c) 2017-2018</p>
*/
//----------------------------------------------------------------------------------
#include "GxViewer.h"
#include "ui_GxViewer.h"
#include<QDebug>

//----------------------------------------------------------------------------------
/**
\Constructor of CGxViewer
*/
//----------------------------------------------------------------------------------
CGxViewer::CGxViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CGxViewer),
    m_pobjAcqThread(NULL),
    m_hDevice(NULL),
    m_pstBaseInfo(NULL),
    m_ui32DeviceNum(0),
    m_bOpen(false),
    m_bAcquisitionStart(false),
    m_bColorFilter(false),
    m_bSaveImage(false),
    m_dImgShowFrameRate(0.0),
    m_pobjShowImgTimer(NULL),
    m_pobjShowFrameRateTimer(NULL),
    m_pobjAcqThread2(NULL),
    m_hDevice2(NULL),
    m_pstBaseInfo2(NULL),
    m_ui32DeviceNum2(0),
    m_bOpen2(false),
    m_bAcquisitionStart2(false),
    m_bColorFilter2(false),
    m_bSaveImage2(false),
    m_dImgShowFrameRate2(0.0),
    m_pobjShowImgTimer2(NULL),
    m_pobjShowFrameRateTimer2(NULL)
{
    ui->setupUi(this);

    this->__SetSystemIcon();

    // Customize type using signal-slot must registed
    qRegisterMetaType<VxInt32>("VxInt32");

    // Setup Image show timer and Frame rate show timer
    m_pobjShowImgTimer = new QTimer(this);
    m_pobjShowFrameRateTimer = new QTimer(this);
    connect(m_pobjShowImgTimer, SIGNAL(timeout()), this, SLOT(slotShowImage()));

    // Connect image save signal and slot non-blocking
    connect(this, SIGNAL(SigSaveImage()), this, SLOT(slotSaveImageFile()), Qt::QueuedConnection);

    m_pobjShowImgTimer2 = new QTimer(this);
    m_pobjShowFrameRateTimer2 = new QTimer(this);
    connect(m_pobjShowImgTimer2, SIGNAL(timeout()), this, SLOT(slotShowImage2()));

    // Connect image save signal and slot non-blocking
    connect(this, SIGNAL(SigSaveImage2()), this, SLOT(slotSaveImageFile2()), Qt::QueuedConnection);

    // Init GxiApi libary
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    emStatus = GXInitLib();
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
    }

    UpdateUI();
}

//----------------------------------------------------------------------------------
/**
\Destructor of CGxViewer
*/
//----------------------------------------------------------------------------------
CGxViewer::~CGxViewer()
{   
    // Stop acquisition thread if acquisition still running
    if (m_bAcquisitionStart)
    {
        m_pobjAcqThread->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread->quit();
        m_pobjAcqThread->wait();
    }

    if (m_bAcquisitionStart2)
    {
        m_pobjAcqThread2->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread2->quit();
        m_pobjAcqThread2->wait();
    }

    // Release acquisition thread object
    RELEASE_ALLOC_MEM(m_pobjAcqThread);

    RELEASE_ALLOC_MEM(m_pobjShowImgTimer);
    RELEASE_ALLOC_MEM(m_pobjShowFrameRateTimer);

    // Release acquisition thread object
    RELEASE_ALLOC_MEM(m_pobjAcqThread2);

    RELEASE_ALLOC_MEM(m_pobjShowImgTimer2);
    RELEASE_ALLOC_MEM(m_pobjShowFrameRateTimer2);

    // Release baseinfo
    RELEASE_ALLOC_ARR(m_pstBaseInfo);
    RELEASE_ALLOC_ARR(m_pstBaseInfo2);

    // Release GxiApi libary
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    emStatus = GXCloseLib();
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
    }

    delete ui;
}

//-------------------------------------------------------------------------
/**
\Set the system icon
\return void
*/
//-------------------------------------------------------------------------
void CGxViewer::__SetSystemIcon()
{
    //Get the exe file path
    QString string_path = QCoreApplication::applicationDirPath();

    //Get the icon path
    QString string_con_path = string_path + QString(":/resources/logo.png");

    //Read the icon
    QPixmap objIconImg(string_con_path);

    //If the icon load fails, the default icon is used.
    if(objIconImg.isNull())
    {
        this->setWindowIcon(QIcon(":/resources/logo.png"));
    }
    else //Load the other icon
    {
        this->setWindowIcon(QIcon(objIconImg));
    }

    return;
}
//----------------------------------------------------------------------------------
/**
\Clear ComboBox Items, close child dialogs
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::ClearUI()
{
    // Clear show image label
    ui->ImageLabel->clear();
    ui->ImageLabel2->clear();

    return;
}

//----------------------------------------------------------------------------------
/**
\ Enable all UI Groups except Camera select group
\param[in]
\param[out]
\return  void
*/
//----------------------------------------------------------------------------------
void CGxViewer::EnableUI()
{
    ui->Capture_Control->setEnabled(true);
}

//----------------------------------------------------------------------------------
/**
\ Disable all UI Groups except Camera select group
\param[in]
\param[out]
\return  void
*/
//----------------------------------------------------------------------------------
void CGxViewer::DisableUI()
{
    ui->Capture_Control->setEnabled(false);
}

//----------------------------------------------------------------------------------
/**
\Update UI items
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::UpdateUI()
{
    ui->UpdateDeviceList->setEnabled(!m_bOpen || !m_bOpen2);
    ui->DeviceList->setEnabled(!m_bOpen);
    ui->DeviceList2->setEnabled(!m_bOpen2);
    ui->OpenDevice->setEnabled(ui->DeviceList->count() > 0 && !m_bOpen);
    ui->OpenDevice2->setEnabled(ui->DeviceList2->count() > 0 && !m_bOpen2);
    ui->CloseDevice->setEnabled(m_bOpen);
    ui->CloseDevice2->setEnabled(m_bOpen2);
//    ui->Capture_Control->setEnabled(m_bOpen);
    ui->actionSaveImage->setEnabled(m_bOpen);
    ui->actionSaveImage2->setEnabled(m_bOpen2);
    ui->StartAcquisition->setEnabled(!m_bAcquisitionStart);
    ui->StartAcquisition2->setEnabled(!m_bAcquisitionStart2);
    ui->StopAcquisition->setEnabled(m_bAcquisitionStart);
    ui->StopAcquisition2->setEnabled(m_bAcquisitionStart2);
    ui->CaptureButton->setEnabled(m_bAcquisitionStart && m_bAcquisitionStart2);

}

//----------------------------------------------------------------------------------
/**
\Setup acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::SetUpAcquisitionThread()
{
    // if Acquisition thread is on Stop acquisition thread
    if (m_pobjAcqThread != NULL)
    {
        m_pobjAcqThread->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread->quit();
        m_pobjAcqThread->wait();

        // Release acquisition thread object
        RELEASE_ALLOC_MEM(m_pobjAcqThread);
    }

    // Instantiation acquisition thread
    try
    {
        m_pobjAcqThread = new CAcquisitionThread;
    }
    catch (std::bad_alloc &e)
    {
        QMessageBox::about(NULL, "Allocate memory error", "Cannot allocate memory, please exit this app!");
        RELEASE_ALLOC_MEM(m_pobjAcqThread);
        return;
    }

    // Connect error signal and error handler
    connect(m_pobjAcqThread, SIGNAL(SigAcquisitionError(QString)), this,
            SLOT(slotAcquisitionErrorHandler(QString)), Qt::QueuedConnection);
    connect(m_pobjAcqThread, SIGNAL(SigImageProcError(VxInt32)), this,
            SLOT(slotImageProcErrorHandler(VxInt32)), Qt::QueuedConnection);

    return;
}

//----------------------------------------------------------------------------------
/**
\Setup acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::SetUpAcquisitionThread2()
{
    // if Acquisition thread is on Stop acquisition thread
    if (m_pobjAcqThread2 != NULL)
    {
        m_pobjAcqThread2->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread2->quit();
        m_pobjAcqThread2->wait();

        // Release acquisition thread object
        RELEASE_ALLOC_MEM(m_pobjAcqThread2);
    }

    // Instantiation acquisition thread
    try
    {
        m_pobjAcqThread2 = new CAcquisitionThread;
    }
    catch (std::bad_alloc &e)
    {
        QMessageBox::about(NULL, "Allocate memory error", "Cannot allocate memory, please exit this app!");
        RELEASE_ALLOC_MEM(m_pobjAcqThread2);
        return;
    }

    // Connect error signal and error handler
    connect(m_pobjAcqThread2, SIGNAL(SigAcquisitionError(QString)), this,
            SLOT(slotAcquisitionErrorHandler(QString)), Qt::QueuedConnection);
    connect(m_pobjAcqThread2, SIGNAL(SigImageProcError(VxInt32)), this,
            SLOT(slotImageProcErrorHandler(VxInt32)), Qt::QueuedConnection);

    return;
}

////----------------------------------------------------------------------------------
///**
//\brief  Save Image menu clicked slot
//\param[in]
//\param[out]
//\return
//*/
////----------------------------------------------------------------------------------
//void CGxViewer::on_actionSaveImage_triggered()
//{
//    // If acquisition not started, do nothing
//    if (!m_bAcquisitionStart)
//    {
//        return;
//    }

//    // Set Save image flag, waiting next showtimer timeout to save image
//    m_bSaveImage = true;

//    return;
//}

////----------------------------------------------------------------------------------
///**
//\brief  Save Image menu clicked slot
//\param[in]
//\param[out]
//\return
//*/
////----------------------------------------------------------------------------------
//void CGxViewer::on_actionSaveImage2_triggered()
//{
//    // If acquisition not started, do nothing
//    if (!m_bAcquisitionStart2)
//    {
//        return;
//    }

//    // Set Save image flag, waiting next showtimer timeout to save image
//    m_bSaveImage2 = true;

//    return;
//}

//----------------------------------------------------------------------------------
/**
\brief  Save image to customize dir(slot)
\param[in]
\param[out]
\return
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotSaveImageFile()
{
    // If acquisition not started, do nothing
    if (!m_bAcquisitionStart)
    {
        return;
    }

    m_objImageForSave.save("left.jpg");

    return;
}

//----------------------------------------------------------------------------------
/**
\brief  Save image to customize dir(slot)
\param[in]
\param[out]
\return
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotSaveImageFile2()
{
    // If acquisition not started, do nothing
    if (!m_bAcquisitionStart2)
    {
        return;
    }

    m_objImageForSave2.save("right.jpg");

    return;
}

//----------------------------------------------------------------------------------
/**
\brief  Enumerate Devcie List, Get baseinfo of these devices
\param[in]
\param[out]
\return
*/
//----------------------------------------------------------------------------------
void CGxViewer::UpdateDeviceList()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    // If base info exist, delete it firstly
    RELEASE_ALLOC_ARR(m_pstBaseInfo);

    // Enumerate Devcie List
    emStatus = GXUpdateDeviceList(&m_ui32DeviceNum, ENUMRATE_TIME_OUT);
    GX_VERIFY(emStatus);

    // If avalible devices enumerated, get base info of enumerate devices
    if(m_ui32DeviceNum > 0)
    {
        // Alloc resourses for device baseinfo
        try
        {
            m_pstBaseInfo = new GX_DEVICE_BASE_INFO[m_ui32DeviceNum];
        }
        catch (std::bad_alloc &e)
        {
            QMessageBox::about(NULL, "Allocate memory error", "Cannot allocate memory, please exit this app!");
            RELEASE_ALLOC_MEM(m_pstBaseInfo);
            return;
        }
        // Set size of function "GXGetAllDeviceBaseInfo"
        size_t nSize = m_ui32DeviceNum * sizeof(GX_DEVICE_BASE_INFO);

        // Get all device baseinfo
        emStatus = GXGetAllDeviceBaseInfo(m_pstBaseInfo, &nSize);
        if (emStatus != GX_STATUS_SUCCESS)
        {
            RELEASE_ALLOC_ARR(m_pstBaseInfo);
            ShowErrorString(emStatus);

            // Reset device number
            m_ui32DeviceNum = 0;

            return;
        }
    }

    return;
}

//----------------------------------------------------------------------------------
/**
\brief  Enumerate Devices(slot)
\param[in]
\param[out]
\return
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_UpdateDeviceList_clicked()
{
    QString szDeviceDisplayName;

    ui->DeviceList->clear();

    // Enumerate Devices
    UpdateDeviceList();

    // If enumerate no device, return
    if (m_ui32DeviceNum == 0)
    {
        // Update Mainwindow
        UpdateUI();
        return;
    }

    // Add items and display items on ComboBox
    for (uint32_t i = 0; i < m_ui32DeviceNum; i++)
    {
        szDeviceDisplayName.sprintf("%s", m_pstBaseInfo[i].szDisplayName);
        ui->DeviceList->addItem(QString(szDeviceDisplayName));
        ui->DeviceList2->addItem(QString(szDeviceDisplayName));
    }

    // Focus on the first device
    ui->DeviceList->setCurrentIndex(0);
    ui->DeviceList2->setCurrentIndex(1);

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Open Device
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::OpenDevice()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    emStatus = GXOpenDeviceByIndex(ui->DeviceList->currentIndex() + 1, &m_hDevice);
    GX_VERIFY(emStatus);

    // isOpen flag set true
    m_bOpen = true;

    return;
}

//----------------------------------------------------------------------------------
/**
\Open Device(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_OpenDevice_clicked()
{
    // Open Device
    OpenDevice();

    // Do not init device or get init params when open failed
    if (!m_bOpen)
    {
        return;
    }

    // Setup acquisition thread
    SetUpAcquisitionThread();


    // Transfer Device handle to acquisition thread class
    m_pobjAcqThread->GetDeviceHandle(m_hDevice);

    // Get MainWindow param from device
    GetDeviceInitParam();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Close Device
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::CloseDevice()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    // Stop Timer
    m_pobjShowImgTimer->stop();
    m_pobjShowFrameRateTimer->stop();

    // Stop acquisition thread before close device if acquisition did not stoped
    if (m_bAcquisitionStart)
    {
        m_pobjAcqThread->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread->quit();
        m_pobjAcqThread->wait();

        // isStart flag reset
        m_bAcquisitionStart = false;
    }

    // Release acquisition thread object
    RELEASE_ALLOC_MEM(m_pobjAcqThread);

    //Close Device
    emStatus = GXCloseDevice(m_hDevice);
    GX_VERIFY(emStatus);

    // isOpen flag reset
    m_bOpen = false;

    // release device handle
    m_hDevice = NULL;

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Close Device(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_CloseDevice_clicked()
{
    // Clear Mainwindow items, especially clear ComboBox
    ClearUI();

    // Close Device
    CloseDevice();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Set device acquisition buffer number.
\param[in]
\param[out]
\return bool    true : Setting success
\               false: Setting fail
*/
//----------------------------------------------------------------------------------
bool CGxViewer::SetAcquisitionBufferNum()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    uint64_t ui64BufferNum = 0;
    int64_t i64PayloadSize = 0;

    // Get device current payload size
    emStatus = GXGetInt(m_hDevice, GX_INT_PAYLOAD_SIZE, &i64PayloadSize);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
        return false;
    }

    // Set buffer quantity of acquisition queue
    if (i64PayloadSize == 0)
    {
        QMessageBox::about(this, "Set Buffer Number", "Set acquisiton buffer number failed : Payload size is 0 !");
        return false;
    }

    // Calculate a reasonable number of Buffers for different payload size
    // Small ROI and high frame rate will requires more acquisition Buffer
    const size_t MAX_MEMORY_SIZE = 8 * 1024 * 1024; // The maximum number of memory bytes available for allocating frame Buffer
    const size_t MIN_BUFFER_NUM  = 5;               // Minimum frame Buffer number
    const size_t MAX_BUFFER_NUM  = 450;             // Maximum frame Buffer number
    ui64BufferNum = MAX_MEMORY_SIZE / i64PayloadSize;
    ui64BufferNum = (ui64BufferNum <= MIN_BUFFER_NUM) ? MIN_BUFFER_NUM : ui64BufferNum;
    ui64BufferNum = (ui64BufferNum >= MAX_BUFFER_NUM) ? MAX_BUFFER_NUM : ui64BufferNum;

    emStatus = GXSetAcqusitionBufferNumber(m_hDevice, ui64BufferNum);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
        return false;
    }

    // Transfer buffer number to acquisition thread class for using GXDQAllBufs
    m_pobjAcqThread->m_ui64AcquisitionBufferNum = ui64BufferNum;

    return true;
}

//----------------------------------------------------------------------------------
/**
\Get parameters from Device and set them into UI items
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::GetDeviceInitParam()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    // Disable all items to avoid user input while initializing
    DisableUI();

    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }

    // Device support frame control or not
    bool bFrameRateControl = false;
    emStatus = GXIsImplemented(m_hDevice, GX_ENUM_ACQUISITION_FRAME_RATE_MODE, &bFrameRateControl);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }

    // Device is a color-camera or not
    bool bColorFilter = false;
    emStatus = GXIsImplemented(m_hDevice, GX_ENUM_PIXEL_COLOR_FILTER, &bColorFilter);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }



    // Initialze success, enable all UI Items,
    EnableUI();

    return;
}


//----------------------------------------------------------------------------------
/**
\Start Acquisition(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_StartAcquisition_clicked()
{
    bool bSetDone = false;
    // Set acquisition buffer number
    bSetDone = SetAcquisitionBufferNum();
    if (!bSetDone)
    {
        return;
    }

    bool bPrepareDone = false;
    // Alloc resource for image acquisition
    bPrepareDone = m_pobjAcqThread->PrepareForShowImg();
    if (!bPrepareDone)
    {
        return;
    }

    // Device start acquisition and start acquisition thread
    StartAcquisition();

    // Do not start timer when acquisition start failed
    if (!m_bAcquisitionStart)
    {
        return;
    }

    // Start image showing timer(Image show frame rate = 1000/nShowTimerInterval)
    // Refresh interval 33ms
    const int nShowTimerInterval = 33;
    m_pobjShowImgTimer->start(nShowTimerInterval);

    // Refresh interval 500ms
    const int nFrameRateTimerInterval = 500;
    m_pobjShowFrameRateTimer->start(nFrameRateTimerInterval);

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Device start acquisition and start acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::StartAcquisition()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    emStatus = GXStreamOn(m_hDevice);
    GX_VERIFY(emStatus);

    // Set acquisition thread run flag
    m_pobjAcqThread->m_bAcquisitionThreadFlag = true;

    // Acquisition thread start
    m_pobjAcqThread->start();

    // isStart flag set true
    m_bAcquisitionStart = true;

    return;
}

//----------------------------------------------------------------------------------
/**
\Stop Acquisition(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_StopAcquisition_clicked()
{    
    // Stop Acquisition
    StopAcquisition();

    // Stop timer
    m_pobjShowImgTimer->stop();
    m_pobjShowFrameRateTimer->stop();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Device stop acquisition and stop acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::StopAcquisition()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    m_pobjAcqThread->m_bAcquisitionThreadFlag = false;
    m_pobjAcqThread->quit();
    m_pobjAcqThread->wait();

    // Turn off stream
    emStatus = GXStreamOff(m_hDevice);
    GX_VERIFY(emStatus);

    // isStart flag set false
    m_bAcquisitionStart = false;

    return;
}

//----------------------------------------------------------------------------------
/**
/Show images acquired and processed(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotShowImage()
{
    // If acquisition did not started
    if (!m_bAcquisitionStart)
    {
        return;
    }

    // Get Image from image show queue, if image show queue is empty, return directly
    QImage* qobjImgShow = m_pobjAcqThread->PopFrontFromShowImageDeque();
    if(qobjImgShow == NULL)
    {
        return;
    }

    if (m_bSaveImage)
    {
        // Deep copy
        m_objImageForSave = qobjImgShow->copy();
        // Reset flag
        m_bSaveImage = false;
        // Emit a signal to save current image
        emit SigSaveImage();
    }

    // Display the image
    QImage objImgScaled = qobjImgShow->scaled(ui->ImageLabel->width(), ui->ImageLabel->height(),
                                           Qt::IgnoreAspectRatio, Qt::FastTransformation);
    ui->ImageLabel->setPixmap(QPixmap::fromImage(objImgScaled));

    // Display is finished, push back image buffer to buffer queue
    m_pobjAcqThread->PushBackToEmptyBufferDeque(qobjImgShow);

    return;
}

//----------------------------------------------------------------------------------
/**
\Open Device
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::OpenDevice2()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    emStatus = GXOpenDeviceByIndex(ui->DeviceList2->currentIndex() + 1, &m_hDevice2);
    GX_VERIFY(emStatus);

    // isOpen flag set true
    m_bOpen2 = true;

    return;
}

//----------------------------------------------------------------------------------
/**
\Open Device(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_OpenDevice2_clicked()
{
    // Open Device
    OpenDevice2();

    // Do not init device or get init params when open failed
    if (!m_bOpen2)
    {
        return;
    }

    // Setup acquisition thread
    SetUpAcquisitionThread2();

    // Setup all Dialogs
    // Transfer Device handle to acquisition thread class
    m_pobjAcqThread2->GetDeviceHandle(m_hDevice2);

    // Get MainWindow param from device
    GetDeviceInitParam2();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Close Device
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::CloseDevice2()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    // Stop Timer
    m_pobjShowImgTimer2->stop();
    m_pobjShowFrameRateTimer2->stop();

    // Stop acquisition thread before close device if acquisition did not stoped
    if (m_bAcquisitionStart2)
    {
        m_pobjAcqThread2->m_bAcquisitionThreadFlag = false;
        m_pobjAcqThread2->quit();
        m_pobjAcqThread2->wait();

        // isStart flag reset
        m_bAcquisitionStart2 = false;
    }

    // Release acquisition thread object
    RELEASE_ALLOC_MEM(m_pobjAcqThread2);

    //Close Device
    emStatus = GXCloseDevice(m_hDevice2);
    GX_VERIFY(emStatus);

    // isOpen flag reset
    m_bOpen2 = false;

    // release device handle
    m_hDevice2 = NULL;

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Close Device(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_CloseDevice2_clicked()
{
    // Clear Mainwindow items, especially clear ComboBox
    ClearUI();

    // Close Device
    CloseDevice2();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Set device acquisition buffer number.
\param[in]
\param[out]
\return bool    true : Setting success
\               false: Setting fail
*/
//----------------------------------------------------------------------------------
bool CGxViewer::SetAcquisitionBufferNum2()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;
    uint64_t ui64BufferNum = 0;
    int64_t i64PayloadSize = 0;

    // Get device current payload size
    emStatus = GXGetInt(m_hDevice2, GX_INT_PAYLOAD_SIZE, &i64PayloadSize);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
        return false;
    }

    // Set buffer quantity of acquisition queue
    if (i64PayloadSize == 0)
    {
        QMessageBox::about(this, "Set Buffer Number", "Set acquisiton buffer number failed : Payload size is 0 !");
        return false;
    }

    // Calculate a reasonable number of Buffers for different payload size
    // Small ROI and high frame rate will requires more acquisition Buffer
    const size_t MAX_MEMORY_SIZE = 8 * 1024 * 1024; // The maximum number of memory bytes available for allocating frame Buffer
    const size_t MIN_BUFFER_NUM  = 5;               // Minimum frame Buffer number
    const size_t MAX_BUFFER_NUM  = 450;             // Maximum frame Buffer number
    ui64BufferNum = MAX_MEMORY_SIZE / i64PayloadSize;
    ui64BufferNum = (ui64BufferNum <= MIN_BUFFER_NUM) ? MIN_BUFFER_NUM : ui64BufferNum;
    ui64BufferNum = (ui64BufferNum >= MAX_BUFFER_NUM) ? MAX_BUFFER_NUM : ui64BufferNum;

    emStatus = GXSetAcqusitionBufferNumber(m_hDevice2, ui64BufferNum);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        ShowErrorString(emStatus);
        return false;
    }

    // Transfer buffer number to acquisition thread class for using GXDQAllBufs
    m_pobjAcqThread2->m_ui64AcquisitionBufferNum = ui64BufferNum;

    return true;
}

//----------------------------------------------------------------------------------
/**
\Get parameters from Device and set them into UI items
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::GetDeviceInitParam2()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    // Disable all items to avoid user input while initializing
    DisableUI();

    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }

    // Device support frame control or not
    bool bFrameRateControl = false;
    emStatus = GXIsImplemented(m_hDevice2, GX_ENUM_ACQUISITION_FRAME_RATE_MODE, &bFrameRateControl);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }

    // Device is a color-camera or not
    bool bColorFilter = false;
    emStatus = GXIsImplemented(m_hDevice2, GX_ENUM_PIXEL_COLOR_FILTER, &bColorFilter);
    if (emStatus != GX_STATUS_SUCCESS)
    {
        CloseDevice();
        GX_VERIFY(emStatus);
    }



    // Initialze success, enable all UI Items,
    EnableUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Start Acquisition(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_StartAcquisition2_clicked()
{
    bool bSetDone2 = false;
    // Set acquisition buffer number
    bSetDone2 = SetAcquisitionBufferNum2();
    if (!bSetDone2)
    {
        return;
    }

    bool bPrepareDone2 = false;
    // Alloc resource for image acquisition
    bPrepareDone2 = m_pobjAcqThread2->PrepareForShowImg();
    if (!bPrepareDone2)
    {
        return;
    }

    // Device start acquisition and start acquisition thread
    StartAcquisition2();

    // Do not start timer when acquisition start failed
    if (!m_bAcquisitionStart2)
    {
        return;
    }

    // Start image showing timer(Image show frame rate = 1000/nShowTimerInterval)
    // Refresh interval 33ms
    const int nShowTimerInterval = 33;
    m_pobjShowImgTimer2->start(nShowTimerInterval);

    // Refresh interval 500ms
    const int nFrameRateTimerInterval = 500;
    m_pobjShowFrameRateTimer2->start(nFrameRateTimerInterval);

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Device start acquisition and start acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::StartAcquisition2()
{
    GX_STATUS emStatus = GX_STATUS_SUCCESS;

    emStatus = GXStreamOn(m_hDevice2);
    GX_VERIFY(emStatus);

    // Set acquisition thread run flag
    m_pobjAcqThread2->m_bAcquisitionThreadFlag = true;

    // Acquisition thread start
    m_pobjAcqThread2->start();

    // isStart flag set true
    m_bAcquisitionStart2 = true;

    return;
}

//----------------------------------------------------------------------------------
/**
\Stop Acquisition(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_StopAcquisition2_clicked()
{
    // Stop Acquisition
    StopAcquisition2();

    // Stop timer
    m_pobjShowImgTimer2->stop();
    m_pobjShowFrameRateTimer2->stop();

    // Update Mainwindow
    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Device stop acquisition and stop acquisition thread
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::StopAcquisition2()
{
    GX_STATUS emStatus2 = GX_STATUS_SUCCESS;

    m_pobjAcqThread2->m_bAcquisitionThreadFlag = false;
    m_pobjAcqThread2->quit();
    m_pobjAcqThread2->wait();

    // Turn off stream
    emStatus2 = GXStreamOff(m_hDevice2);
    GX_VERIFY(emStatus2);

    // isStart flag set false
    m_bAcquisitionStart2 = false;

    return;
}

//----------------------------------------------------------------------------------
/**
/Show images acquired and processed(slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotShowImage2()
{
    // If acquisition did not started
    if (!m_bAcquisitionStart2)
    {
        return;
    }

    // Get Image from image show queue, if image show queue is empty, return directly
    QImage* qobjImgShow2 = m_pobjAcqThread2->PopFrontFromShowImageDeque();
    if(qobjImgShow2 == NULL)
    {
        return;
    }

    if (m_bSaveImage2)
    {
        // Deep copy
        m_objImageForSave2 = qobjImgShow2->copy();
        // Reset flag
        m_bSaveImage2 = false;
        // Emit a signal to save current image
        emit SigSaveImage2();
    }

    // Display the image
    QImage objImgScaled2 = qobjImgShow2->scaled(ui->ImageLabel2->width(), ui->ImageLabel2->height(),
                                           Qt::IgnoreAspectRatio, Qt::FastTransformation);
    ui->ImageLabel2->setPixmap(QPixmap::fromImage(objImgScaled2));

    // Display is finished, push back image buffer to buffer queue
    m_pobjAcqThread2->PushBackToEmptyBufferDeque(qobjImgShow2);

    return;
}


//----------------------------------------------------------------------------------
/**
\Capture (slot)
\param[in]
\param[out]
\return void
*/
//----------------------------------------------------------------------------------
void CGxViewer::on_CaptureButton_clicked()
{

    m_bSaveImage = true;
    m_bSaveImage2 = true;
    slotSaveImageFile();
    slotSaveImageFile2();

    qDebug() << "Image saved successfully";

    return;
}



//----------------------------------------------------------------------------------
/**
\Refresh Main window when execute usersetload
\param[in]
\param[out]
\return  void
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotRefreshMainWindow()
{
    // Clear Items in ComboBox
    this->GetDeviceInitParam();

    return;
}

//----------------------------------------------------------------------------------
/**
\Get error from acquisition thread and show error message
\param[in]      szErrorString  Error string from acquisition thread
\param[out]
\return  void
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotAcquisitionErrorHandler(QString szErrorString)
{
    // Show error and stop acquisition
    QMessageBox::about(NULL, "Acquisition Error", szErrorString);

    StopAcquisition();

    UpdateUI();

    return;
}

//----------------------------------------------------------------------------------
/**
\Get error from image processing and show error message
\param[in]     emStatus     Error code from image processing
\param[out]
\return  void
*/
//----------------------------------------------------------------------------------
void CGxViewer::slotImageProcErrorHandler(VxInt32 emStatus)
{
    // Show error and stop acquisition : Error code 0 means pixel format not support
    QMessageBox::about(NULL, "Image Process Error", QString("Error : Image Processing Failed, Error code : %1").arg(emStatus));
    StopAcquisition();

    UpdateUI();

    return;
}
