//--------------------------------------------------------------------------------
/**
\file     GxViewer.h
\brief    CGxViewer Class declaration file

\version  v1.0.1807.9271
\date     2018-07-27

<p>Copyright (c) 2017-2018</p>
*/
//----------------------------------------------------------------------------------
#ifndef GXVIEWER_H
#define GXVIEWER_H

#include "Common.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QPixmap>
#include <deque>

#include "AcquisitionThread.h"

#define ENUMRATE_TIME_OUT       200

namespace Ui {
    class CGxViewer;
}

class CGxViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit CGxViewer(QWidget *parent = 0);
    ~CGxViewer();

private:
    /// Set the icon for the sample program
    void __SetSystemIcon();

    /// Clear Mainwindow items
    void ClearUI();

    /// Enable all UI Groups
    void EnableUI();

    /// Disable all UI Groups
    void DisableUI();

    /// Update all items status on MainWindow
    void UpdateUI();

    /// Update device list
    void UpdateDeviceList();

    /// Setup acquisition thread
    void SetUpAcquisitionThread();
    void SetUpAcquisitionThread2();

    /// Open device selected
    void OpenDevice();
    void OpenDevice2();

    /// Close device opened
    void CloseDevice();
    void CloseDevice2();

    /// Get device info and show it on text label
    void ShowDeviceInfo();
    void ShowDeviceInfo2();

    /// Set device acquisition buffer number.
    bool SetAcquisitionBufferNum();
    bool SetAcquisitionBufferNum2();

    /// Get parameters from opened device
    void GetDeviceInitParam();
    void GetDeviceInitParam2();

    /// Device start acquisition and start acquisition thread
    void StartAcquisition();
    void StartAcquisition2();

    /// Device stop acquisition and stop acquisition thread
    void StopAcquisition();
    void StopAcquisition2();


    Ui::CGxViewer       *ui;                        ///< User Interface

    CAcquisitionThread  *m_pobjAcqThread;           ///< Child image acquisition and process thread

    GX_DEV_HANDLE        m_hDevice;                 ///< Device Handle
    GX_DEVICE_BASE_INFO *m_pstBaseInfo;             ///< Pointer struct of Device info
    uint32_t             m_ui32DeviceNum;           ///< Device number enumerated

    bool                 m_bOpen;                   ///< Flag : camera is opened or not
    bool                 m_bAcquisitionStart;       ///< Flag : camera is acquiring or not
    bool                 m_bColorFilter;            ///< Flag : Support color pixel format or not
    bool                 m_bSaveImage;              ///< Flag : Save one image when it is true

    QImage               m_objImageForSave;         ///< For image saving
    double               m_dImgShowFrameRate;       ///< Framerate of image show

    QTimer              *m_pobjShowImgTimer;        ///< Timer of Show Image
    QTimer              *m_pobjShowFrameRateTimer;  ///< Timer of show Framerate

    //New camera (2)

    CAcquisitionThread  *m_pobjAcqThread2;           ///< Child image acquisition and process thread

    GX_DEV_HANDLE        m_hDevice2;                 ///< Device Handle
    GX_DEVICE_BASE_INFO *m_pstBaseInfo2;             ///< Pointer struct of Device info
    uint32_t             m_ui32DeviceNum2;           ///< Device number enumerated

    bool                 m_bOpen2;                   ///< Flag : camera is opened or not
    bool                 m_bAcquisitionStart2;       ///< Flag : camera is acquiring or not
    bool                 m_bColorFilter2;            ///< Flag : Support color pixel format or not
    bool                 m_bSaveImage2;              ///< Flag : Save one image when it is true

    QImage               m_objImageForSave2;         ///< For image saving
    double               m_dImgShowFrameRate2;       ///< Framerate of image show

    QTimer              *m_pobjShowImgTimer2;        ///< Timer of Show Image
    QTimer              *m_pobjShowFrameRateTimer2;  ///< Timer of show Framerate

private slots:
    /// Open SaveImage dialog
    void on_actionSaveImage_triggered();
    void on_actionSaveImage2_triggered();

    /// Save image to customize dir
    void slotSaveImageFile();
    void slotSaveImageFile2();

    /// Update device list
    void on_UpdateDeviceList_clicked();

    /// Open device selected
    void on_OpenDevice_clicked();
    void on_OpenDevice2_clicked();

    /// Close device opened
    void on_CloseDevice_clicked();
    void on_CloseDevice2_clicked();

    /// Start Acqusition
    void on_StartAcquisition_clicked();
    void on_StartAcquisition2_clicked();

    /// Stop Acquisition
    void on_StopAcquisition_clicked();
    void on_StopAcquisition2_clicked();

    void on_CaptureButton_clicked();

    /// Show images acquired and processed
    void slotShowImage();
    void slotShowImage2();

    /// Get error from acquisition thread and show error message
    void slotAcquisitionErrorHandler(QString);

    /// Get error from image processing and show error message
    void slotImageProcErrorHandler(VxInt32);

    /// Refresh Main window when execute usersetload
    void slotRefreshMainWindow();

signals:
    /// Emit save image signal
    void SigSaveImage();
    void SigSaveImage2();
};

#endif // GXVIEWERDEMO_H
